//
//  Model.swift
//  StreamCinema
//
//  Created by SCC on 23/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya

protocol Model: Codable {
    static func empty<T: Model>() -> T
}
