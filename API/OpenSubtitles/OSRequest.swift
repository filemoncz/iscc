//
//  OSRequest.swift
//  StreamCinema.atv
//
//  Created by SCC on 05/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//
//https://www.opensubtitles.com/docs/api/html/index.htm

import UIKit
import Alamofire
import Moya

extension Provider {
    static let osRequest: MoyaProvider<OSRequest> = NetworkingClient.provider()
}

enum OSRequest: TargetType {
    
    case login(name:String, pass:String)
    case findWith(imdbid:String, lang:[String])
    case download(id:String, name:String)
    
    var baseURL: URL {
        return URLSettings.OpenSubtitles
    }
    
    var method: Moya.Method {
        switch self {
        case .login:
            return .post
        case .findWith:
            return .get
        case .download:
            return .post
        }
    }
    
    var headers: [String: String]? {
        var headers = ["Content-Type":"application/json"]
        headers["Api-Key"] = "d9zuMXbhVIwtVVoNxPMR0QjFMrJH7dWe"
        switch self {
        case .login:
            break
        case .findWith:
            headers["Authorization"] = LoginManager.osTokenHash
        case .download:
            headers["Authorization"] = LoginManager.osTokenHash
        }
        return headers
    }
    
    var path: String {
        switch self {
        case .login:
            return self.loginPath
        case .findWith(imdbid: _, lang: _):
            return self.findPaht
        case .download:
            return self.downloadPath
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameterEncoding: Moya.ParameterEncoding {
        switch self {
        default:
            return URLEncoding.default
        }
    }
    
    var task: Task {
        switch self {
        case .login(name:let name, pass:let pass):
            return .requestParameters(parameters: ["username": name, "password": pass], encoding: JSONEncoding.default)
        case .findWith(imdbid: let imdbid, lang: let lang):
            let test = URLEncoding(destination: .methodDependent, arrayEncoding: .noBrackets, boolEncoding: .numeric)
            return .requestParameters(parameters: ["imdb_id": imdbid, "languages": lang.joined(separator: ",")], encoding: test)
        case .download(id: let id, name: let name):
            let data = ["file_id":id, "file_name":name, "sub_format":"srt"]
            return .requestParameters(parameters: data, encoding: JSONEncoding.default)
        }
    }
}

extension OSRequest {
    var loginPath: String {
        return "/login/"
    }
    var findPaht: String {
        return "/subtitles"
    }
    var downloadPath: String {
        return "/download/"
    }
}

