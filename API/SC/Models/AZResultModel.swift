//
//  AZResultModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

struct AZResults: DataConvertible, Hashable, Model {
    let data: [AZResult]?
    let total: Int?
    
    static func empty<T>() -> T where T: Model {
        let filterResult = AZResults(data:[], total:0)
        guard let emptyFilterResult = filterResult as?T else {
            fatalError()
        }
        return emptyFilterResult
    }
}

struct AZResult: DataConvertible, Hashable, Model {
    let value: String?
    let count: Int?
    var imageUrl: URL?
        
    enum CodingKeys: String, CodingKey {
        case value = "key"
        case count = "doc_count"
    }

    static func empty<T>() -> T where T: Model {
        let filterResult = AZResult(value: nil, count: nil)
        guard let emptyFilterResult = filterResult as?T else {
            fatalError()
        }
        return emptyFilterResult
    }
}
