//
//  GenereModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 21/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

protocol FindingValue {
    var value: String { get }
    var string: String { get }
}

enum Genere: String, CaseIterable, FindingValue {
    case PORNOGRAPHIC = "Pornographic"
    case ACTION = "Action"
    case ANIMATED = "Animated"
    case ANIMATION = "Animation"
    case ADVENTURE = "Adventure"
    case BIOGRAPHICAL = "Biographical"
    case CATASTROPHIC = "Catastrophic"
    case COMEDY = "Comedy"
    case COMPETITION = "Competition"
    case CRIME = "Crime"
    case DOCUMENTARY = "Documentary"
    case FAIRYTALE = "Fairytale"
    case DRAMA = "Drama"
    case FAMILY = "Family"
    case FANTASY = "Fantasy"
    case HISTORICAL = "Historical"
    case HORROR = "Horror"
    case IMAX = "IMAX"
    case EDUCATIONAL = "Educational"
    case MUSIC = "Music"
    case JOURNALISTIC = "Journalistic"
    case MILITARY = "Military"
    case MUSICAL = "Musical"
    case MYSTERIOUS = "Mysterious"
    case PSYCHOLOGICAL = "Psychological"
    case REALITY = "Reality"
    case ROMANTIC = "Romantic"
    case SCI_FI = "Sci-Fi"
    case SHORT = "Short"
    case SPORTS = "Sports"
    case STAND_UP = "Stand-Up"
    case TALK_SHOW = "Talk-Show"
    case TELENOVELA = "Telenovela"
    case THRILLER = "Thriller"
    case TRAVEL = "Travel"
    case WESTERN = "Western"
    case EROTIC = "Erotic"
    case Fairy_Tale = "Fairy Tale"
    
    var value: String {
        rawValue
    }

    var string:String {
        String(localizationKey:  self.rawValue)
    }
    
    static var explicitGenere:[String] {
        return [Genere.PORNOGRAPHIC.rawValue,Genere.EROTIC.rawValue]
    }
    
    static var explicit:[Genere] {
        return [Genere.PORNOGRAPHIC, Genere.EROTIC]
    }
    
    static var allTvShows:[Genere] {
        return [.ACTION,
         .ADVENTURE,
         .ANIMATED,
         .ANIMATION,
         .BIOGRAPHICAL,
         .CATASTROPHIC,
         .COMEDY,
         .CRIME,
         .DOCUMENTARY,
         .FAMILY,
         .FANTASY,
         .HORROR,
         .JOURNALISTIC,
         .MUSIC,
         .MUSICAL,
         .MYSTERIOUS,
         .PORNOGRAPHIC,
         .PSYCHOLOGICAL,
         .REALITY,
         .ROMANTIC,
         .SCI_FI,
         .SHORT,
         .SPORTS,
         .THRILLER,
         .WESTERN].sorted { (left, right) -> Bool in
            return left.string.lowercased().compare(right.string.lowercased()) == .orderedAscending
        }
    }
    
    static var allMovie:[Genere] {
        return [.ACTION,
         .ADVENTURE,
         .ANIMATED,
        .ANIMATION,
        .BIOGRAPHICAL,
        .CATASTROPHIC,
        .COMEDY,
        .CRIME,
        .DOCUMENTARY,
        .DRAMA,
        .EROTIC,
        .FAMILY,
        .FANTASY,
        .HORROR,
        .IMAX,
        .JOURNALISTIC,
        .MUSIC,
        .MUSICAL,
        .MYSTERIOUS,
        .PORNOGRAPHIC,
        .PSYCHOLOGICAL,
        .ROMANTIC,
        .SCI_FI,
        .SHORT,
        .SPORTS,
        .THRILLER,
        .WESTERN].sorted { (left, right) -> Bool in
            return left.string.lowercased().compare(right.string.lowercased()) == .orderedAscending
        }
    }
}
