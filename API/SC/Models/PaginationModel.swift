//
//  PaginationModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

// MARK: - Pagination
struct Pagination: DataConvertible, Hashable, Model {
    let next, prev: String?
    let page, pageCount, from, limit: Int
    
    static func empty<T>() -> T where T: Model {
        let pagination = Pagination(next: "", prev: "", page: 0, pageCount: 0, from: 0, limit: 0)
        guard let emptyPagination = pagination as?T else {
            fatalError()
        }
        return emptyPagination
    }
}
