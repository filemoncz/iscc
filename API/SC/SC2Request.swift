//
//  SC2_ApiRequest.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Alamofire
import Moya

extension Provider {
    static let sc2Request: MoyaProvider<SC2Request> = NetworkingClient.provider()
}
///https://plugin.sc2.zone/api/media/filter/custom?config=%7B%22bool%22%3A%7B%22must%22%3A%5B%7B%22match%22%3A%7B%22cast.name%22%3A%22Adelaide%20Kane%22%7D%7D%5D%7D%7D&access_token=5ze3qiq6lmoartnpktvl&sortConfig=%7B%22ratings.dateadded%22%3A%7B%22order%22%20%3A%20%22asc%22%7D%7D
///config={"bool":{"must":[{"match":{"cast.name":"Adelaide Kane"}}]}}
///sortConfig={"info_labels. premiered":{"order" : "asc"}}
///https://plugin.sc2.zone/api/media/filter/custom?config=%7B%22bool%22%3A%7B%22must%22%3A%5B%7B%22match%22%3A%7B%22cast.name%22%3A%22Adelaide%20Kane%22%7D%7D%5D%7D%7D&access_token=5ze3qiq6lmoartnpktvl
///https://plugin.sc2.zone/api/media/filter/custom?config=%7B%22bool%22%3A%7B%22must%22%3A%5B%7B%22match%22%3A%7B%22i18n_info_labels.title%22%3A%22a%22%7D%7D%5D%7D%7D&access_token=5ze3qiq6lmoartnpktvl
///https://plugin.sc2.zone/api/media/filter/startsWithSimple/count/titles?type=movie&value=
///http://plugin.sc2.zone/api/stream/probe/L932p6cp3S
///https://plugin.sc2.zone/api/media/tv/2020-08-14T00:00:00.000Z/program?access_token=5ze3qiq6lmoartnpktvl
enum SC2Request: TargetType {
    
    case getMedia(mediaID:String)
    case getMediaFrom(Service:FilterService, mediaID:String)
    case filter(with:FilterModel)
    case getStream(mediaID:String)
    case azSearch(filter:FilterModel)
    case setAsPlayed(mediaID:String)
    case tvProgram(station:String, date:String, startIndex: Int, offSet: Int)
    case analytics(url:URL, a:String)
    case findMoviesBy(customFilter: CustomFilterModel)
    
    
    var baseURL: URL {
        return URLSettings.sc2Url
    }
    
    var method: Moya.Method {
        switch self {
        case .setAsPlayed,
             .analytics:
            return .post
        case .filter,
             .azSearch,
             .getMedia,
             .getMediaFrom,
             .tvProgram,
             .getStream,
             .findMoviesBy:
            return .get
        }
    }
    
    var headers: [String: String]? {
//        var extraHeader: [String: String] = [:]
//        switch self {
//        case .azSearch(let filter), .filter(let filter):
//            extraHeader["from"] = "\(filter.startIndex)"
//            extraHeader["size"] = "\(filter.offSet)"
//        case .findMoviesBy(let filter):
//            extraHeader["from"] = "\(filter.startIndex)"
//            extraHeader["size"] = "\(filter.offSet)"
//        default:
//            break
//        }
        if let uuid = UIDevice.current.identifierForVendor?.uuidString,
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            var header = ["X-Uuid":uuid, "User-Agent":"com.streamcinemacommunity.atv" + "_" + appVersion]
//            header.merge(extraHeader, uniquingKeysWith: { (first, _) in first })
            return header
        }
        return nil
    }
    
    var path: String {
        switch self {
        case .azSearch(let model):
            guard let count = model.count?.rawValue else { return "" }
            if model.isV2Filter {
                return self.filterPath + model.name.rawValue + self.azSearch + count
            }
            return self.oldFilterPath + model.name.rawValue + self.azSearch + count
        case .filter(let filterModel):
            if filterModel.isV2Filter {
                return self.filterPath + filterModel.name.rawValue
            }
            return self.oldFilterPath + filterModel.name.rawValue
        case .getMedia(let mediaID):
            return self.mediaPath + mediaID
        case .getMediaFrom(let service, let mediaID):
            return self.servicePath + service.rawValue + "/" + mediaID
        case .getStream(let mediaID):
            return self.mediaPath + mediaID + self.streamsPath
        case .setAsPlayed(mediaID: let mediaID):
            return self.playedPath + mediaID
        case .tvProgram(station: _, date: let date, startIndex: _, offSet: _):
            return self.tvProgram + date + "T00:00:00.000Z" + "/program"
        case .analytics:
            guard let token = Bundle.main.infoDictionary?["CFBundleSCC"] as? String else { return self.analytics }
            return self.analytics + "?access_token=" + token
        case .findMoviesBy:
            return self.filterPath + "custom"
        }
    }
    
    var task: Task {
        switch self {
        case .filter(let model),
             .azSearch(let model):
            let test = URLEncoding(destination: .methodDependent, arrayEncoding: .noBrackets, boolEncoding: .numeric)
            return .requestParameters(parameters: model.getDictionnary(), encoding: test)
        case .getMedia,
             .getMediaFrom,
             .getStream:
            guard let token = Bundle.main.infoDictionary?["CFBundleSCC"]  else { return .requestPlain }
            let test = URLEncoding(destination: .methodDependent, arrayEncoding: .noBrackets, boolEncoding: .numeric)
            return .requestParameters(parameters: ["access_token":token], encoding: test)
        case .setAsPlayed:
            guard let token = Bundle.main.infoDictionary?["CFBundleSCC"]  else { return .requestPlain }
            return .requestParameters(parameters: ["access_token":token], encoding: URLEncoding.queryString)
        case .tvProgram(station: let station, date: _, startIndex: let startIndex, offSet: let offSet):
            guard let token = Bundle.main.infoDictionary?["CFBundleSCC"]  else { return .requestPlain }
            return .requestParameters(parameters: ["access_token":token, "station":station, "from":startIndex, "size":offSet], encoding: URLEncoding.queryString)
        case .analytics(url: let url, let a):
            return .requestParameters(parameters: ["url":url, "a":a], encoding: JSONEncoding.default)
        case .findMoviesBy(let customFilterModel):
            guard let token = Bundle.main.infoDictionary?["CFBundleSCC"]  else { return .requestPlain }
            let test = URLEncoding(destination: .methodDependent, arrayEncoding: .noBrackets, boolEncoding: .numeric)
            return .requestParameters(parameters: ["access_token":token,
                                                   "sortConfig":customFilterModel.sortConfig,
                                                   "config":customFilterModel.config,
                                                   "from":customFilterModel.startIndex,
                                                   "size":customFilterModel.offSet],
                                      encoding: test)
        }
    }
}

extension SC2Request {
    var playedPath: String {
        return "/media/played/"
    }
    var mediaPath: String {
        return "/media/"
    }
    var servicePath: String {
        return "/media/service/"
    }
    var filterPath: String {
        return "/media/filter/v2/"
    }
    var oldFilterPath: String {
        return "/media/filter/"
    }
    var streamsPath: String {
        return "/streams"
    }
    var azSearch: String {
        return "/count/"
    }
    var tvProgram: String {
        return "media/tv/"
    }
    var analytics: String {
        return "stats/analytics"
    }
}
