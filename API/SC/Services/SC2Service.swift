//
//  FindService.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import Combine
import TraktKit

final class SC2Service: SCCService {

    //MARK: - Sources Endpoind
    public func setAsPlayed(mediaID: String) -> AnyPublisher<Void, Error> {
        apiClient
            .requestWithoutMapping(request: SC2Request.setAsPlayed(mediaID: mediaID))
            .map { _ in () }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
    
    public func sendAnalytics(url:URL) {
        guard let name = LoginManager.name else { return }
        apiClient
            .request(SC2Request.analytics(url: url, a: name))
    }

//    public func getTvProgram(for station: String, date: String, page: Int) -> AnyPublisher<FilterResult, Error> {
//        apiClient
//            .request(request: SC2Request.tvProgram(station: station, date: date, page: page), model: FilterResult.self)
//    }
    
    //NOT WORK! wrong api
    @available(*, deprecated)
    public func getMovie(tmdb: Int) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: .initTmdbFilter(value: tmdb, startIndex: 0))
    }
    
    //NOT WORK! wrong api
    @available(*, deprecated)
    public func getMovie(imdbID: String) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: .initImdbFilter(value: imdbID, startIndex: 0))
    }
    
    public func getMovies(traktID: [String], startIndex: Int, offSet: Int) -> AnyPublisher<SCCMovieResult, Error> {
        if traktID.count == 0 {
            return Just(SCCMovieResult())
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }
        return filter(with: .initTaktFilter(value: traktID, startIndex: startIndex))
    }
    
    public func getStreams(for mediaID: String) -> AnyPublisher<VideoStreams, Error> {
        return apiClient
            .requestArr(request: SC2Request.getStream(mediaID: mediaID), model: VideoStreams.self)
    }
    
    public func getMovieScc(by movieID:String, type: FilterType) -> AnyPublisher<SourceInfo, Error> {
        apiClient
            .request(request: SC2Request.getMedia(mediaID: movieID), model: SourceInfo.self)
    }

    public func getNext(episode:String, season: String, root_parent: String) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: .getNext(episode: episode, season: season, root_parent:root_parent, startIndex: 0))
    }
    
    public func getAllStudios(model: FilterModel) -> AnyPublisher<AZResults, Error> {
        apiClient
            .request(request: SC2Request.azSearch(filter: model), model: AZResults.self)
    }
    
    public func getMovies(type: FilterType, for item: MenuItem, startIndex: Int,  offSet: Int = 100) -> AnyPublisher<SCCMovieResult, Error> {
        switch item {
        case .dubbed:
            return filter(with: .lastReleasedDubbed(type: type, startIndex: startIndex, offSet: offSet))
        case .lastAdded:
            return filter(with: .lastAddedFilter(type: type, startIndex: startIndex, offSet: offSet))
        case .news:
            return filter(with: .newsFilter(type: type, startIndex: startIndex, offSet: offSet))
        case .popular:
            return filter(with: .popular(type: type, startIndex: startIndex, offSet: offSet))
        case .watched:
            return getWatched(type: type, startIndex: startIndex, offSet:  offSet)
        case .watchedList:
            return getWatched(type: type, startIndex: startIndex, offSet: offSet, entity: .sccWatchList)
        case .mostWatched:
            return filter(with: .lastMostWatched(type: type, startIndex: startIndex, offSet: offSet))
        case .traktWatchList:
            return getTraktWatchList(startIndex: startIndex, offSet: offSet)
        case .trending:
            return filter(with: .trending(type: type, startIndex: startIndex, offSet: offSet))
        case .traktHistory:
            return getTraktHistori(startIndex: startIndex, offSet: offSet)
        case .genre, .friendsAdvise:
            return Fail(error: SC2Error.genreMovieFilterNotSpecified).eraseToAnyPublisher()
        case .lastAddedDubbed:
            return filter(with: .lastAddedDubbedFilter(type: type, startIndex: startIndex, offSet: offSet))
        case .christmas:
            return getMoviesFromTaktList(startIndex: startIndex, offSet: offSet)
        }
    }
    
    private func getMoviesFromTaktList(_ username: String = "barsaky",
                                       listID: String = "vianoce-christmas",
                                       startIndex: Int,
                                       offSet: Int) -> AnyPublisher<SCCMovieResult, Error>
    {
        getMoviesFromList(username,listID: listID)
            .flatMap { [weak self] ids -> AnyPublisher<SCCMovieResult, Error>  in
                guard let `self` = self else { return Fail(error: SC2Error.unknownTraktError).eraseToAnyPublisher() }
                return self.getMovies(traktID: ids, startIndex: startIndex, offSet: offSet)
            }
            .eraseToAnyPublisher()
    }
    
    private func getMoviesFromList(_ username: String = "barsaky",listID: String = "vianoce-christmas") -> AnyPublisher<[String], Error> {
        let subject: PassthroughSubject<[String], Error> = PassthroughSubject()
        
        TraktManager.sharedManager.getItemsForCustomList(username: username, listID: listID) { response in
            switch response {
            case .success(objects: let objects):
                let traktIds = objects.compactMap { item in
                    if let traktID = item.movie?.ids.trakt {
                        return "\(item.type):\(traktID)"
                    }
                    if let traktID = item.show?.ids.trakt {
                        return "\(item.type):\(traktID)"
                    }
                    return nil
                }
                subject.send(traktIds)
            case .error(error: let error):
                let error = error ?? SC2Error.unknownTraktError
                subject.send(completion: .failure(error))
            }
        }
        
        return subject.first().eraseToAnyPublisher()
    }
    
    private func getTraktId(_ username: String = "barsaky",listID: String = "vianoce-christmas") -> AnyPublisher<Int, Error> {
        let subject: PassthroughSubject<Int, Error> = PassthroughSubject()
        
        TraktManager.sharedManager.getCustomList(username: username,listID: listID) { response in
            switch response {
            case .success(object: let object):
                subject.send(object.ids.trakt)
            case .error(error: let error):
                let error = error ?? SC2Error.unknownTraktError
                subject.send(completion: .failure(error))
            }
        }
        
        return subject.first().eraseToAnyPublisher()
    }

    private func getTraktHistori(startIndex: Int, offSet: Int) -> AnyPublisher<SCCMovieResult, Error> {

        getHistoryTraktData()
            .flatMap { [weak self] ids -> AnyPublisher<SCCMovieResult, Error>  in
                guard let `self` = self else { return Fail(error: SC2Error.unknownTraktError).eraseToAnyPublisher() }
                return self.getMovies(traktID: ids, startIndex: startIndex, offSet: offSet)
            }
            .eraseToAnyPublisher()
    }
    
    private func getHistoryTraktData() -> AnyPublisher<[String], Error> {

        let subject: PassthroughSubject<[String], Error> = PassthroughSubject()

        TraktManager.sharedManager.getHistory { response in
            switch response {
            case .success(objects: let objects, currentPage: let currentPage, limit: let limit):
                Log.write("getHistory.success(objects: \(objects), currentPage: \(currentPage), limit: \(limit)")
                var ids:[String] = []
                for traktData in objects {
                    if let traktID = traktData.movie?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                    if let traktID = traktData.show?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                }
                subject.send(ids)
            case .error(error: let err):
                let error = err ?? SC2Error.unknownTraktError
                subject.send(completion: .failure(error))
            }
        }
        return subject.first().eraseToAnyPublisher()
    }

    private func getTraktWatchList(startIndex: Int, offSet: Int) -> AnyPublisher<SCCMovieResult, Error> {
        getWatchlistTraktData()
            .flatMap { [weak self] ids -> AnyPublisher<SCCMovieResult, Error> in
                guard let `self` = self else { return Fail(error: SC2Error.unknownTraktError).eraseToAnyPublisher() }
                return self.getMovies(traktID: ids, startIndex: startIndex, offSet: offSet)
            }.eraseToAnyPublisher()
    }
    
    private func getWatchlistTraktData() -> AnyPublisher<[String], Error>{

        let subject: PassthroughSubject<[String], Error> = PassthroughSubject()
        TraktManager.sharedManager.getWatchlist(watchType: .Movies) { response in
            switch response {
            case .error(error: let err):
                let error = err ?? SC2Error.unknownTraktError
                subject.send(completion: .failure(error))
            case .success(let objects, _, _):
                var ids:[String] = []
                for traktData in objects {
                    if let traktID = traktData.movie?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                    if let traktID = traktData.show?.ids.trakt {
                        ids.append("\(traktData.type):\(traktID)")
                    }
                }
                subject.send(ids)
            }
        }
        return subject.first().eraseToAnyPublisher()
    }

    public func getSeries(for mediaID:String, showID: IDs?, seasonID: IDs?, startIndex: Int, offSet: Int) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: FilterModel.seasonFilter(mediaID:mediaID, startIndex: startIndex, offSet: offSet))
            .map {
                var resultData = $0
                guard resultData.data.count > 0 else { return resultData }
                for i in 0...resultData.data.count - 1  {
                   if let showID = showID {
                       resultData.data[i].rootIDs = showID
                   }
                   if let seasonID = seasonID {
                       resultData.data[i].seasonIDs = seasonID
                   }
               }
             return resultData
            }.eraseToAnyPublisher()
    }
    
    public func getCsfdMedia(for csfdIDs:[String], type: FilterType, startIndex: Int, offSet: Int, sort:FilterSort? = nil) -> AnyPublisher<SCCMovieResult, Error>  {
        filter(with: FilterModel.csfdFillter(serviceID: csfdIDs, type: type, sort:sort, startIndex: startIndex, offSet: offSet))
    }
    
    public func getMedia(by ids:[String], type: FilterType, startIndex: Int, offSet: Int, sort:FilterSort? = nil) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: FilterModel.sccIDsFilter(sccIDs: ids, type: type, sort:sort, startIndex: startIndex, offSet: offSet))
    }
    
    public func getMediaAsync(by ids:[String],
                              type: FilterType,
                              startIndex: Int,
                              offSet: Int,
                              sort:FilterSort? = nil) async throws -> SCCMovieResult {
        let target = FilterModel.sccIDsFilter(sccIDs: ids, type: type, sort:sort, startIndex: startIndex, offSet: offSet)
        
        return try await filter(with: target)
    }

    private func getWatched(type: FilterType, startIndex: Int, offSet: Int, entity: WatchedEntity = .scc) -> AnyPublisher<SCCMovieResult, Error> {
        let data = WatchedData.getAllHistoryData(for: type, entity: entity)
        let items = Array(data.prefix(200))
        return getMedia(with: items, type: type, startIndex: startIndex, offSet: offSet)
    }
    
    private func getMedia(with data: [SCCMovie], type: FilterType, startIndex: Int, offSet: Int = 100) -> AnyPublisher<SCCMovieResult, Error> {
        if data.count == 0 {
            return Just(SCCMovieResult())
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()

        }
        var allSccIDs:[String] = []
        for watched in data {
            if let sccID = watched.rootIDs?.sccID {
                allSccIDs.append(sccID)
            }
//            if let sccID = watched.rootIDs?.sccID {
//                allSccIDs.append(sccID)
//            }
        }
        let filter = FilterModel.sccIDsFilter(sccIDs: allSccIDs, type: type, sort: nil, startIndex: startIndex, offSet: offSet)
        let result = self.filter(with:filter)
        return result
    }

    public func genre(with genre:FindingValue, type:FilterType, startIndex: Int, offSet: Int = 100) -> AnyPublisher<SCCMovieResult, Error> {
        let filterModel = FilterModel.genere(value: genre, type: type, startIndex: startIndex, offSet: offSet)
        return filter(with: filterModel)
    }
    
    //MARK: - FilterEndpoint
    public func customSearch(wtih model:CustomFilterModel) -> AnyPublisher<SCCMovieResult, Error> {
        apiClient
            .request(request: SC2Request.findMoviesBy(customFilter: model), model: FilterResult.self)
            .map { SCCMovieResult($0) }
            .eraseToAnyPublisher()
    }
    public func search(wtih model:FilterModel) -> AnyPublisher<SCCMovieResult, Error> {
        filter(with: model)
    }
    
    public func azSearch(model: FilterModel) -> AnyPublisher<AZResults, Error> {
        apiClient
            .request(request: SC2Request.azSearch(filter: model), model: AZResults.self)
    }

    private func filter(with model: FilterModel) -> AnyPublisher<SCCMovieResult, Error> {
        let request = SC2Request.filter(with: model)
        return apiClient.request(request: request, model: FilterResult.self)
            .map {
                SCCMovieResult($0)
            }
            .eraseToAnyPublisher()
    }
    
    private func filter(with model: FilterModel) async throws -> SCCMovieResult {
        let target = SC2Request.filter(with: model)
        do {
            let result = try await apiClient.requestAsync(target, model: FilterResult.self)
            return SCCMovieResult(result)
        } catch  {
            throw error
        }
    }
    
    private func analytics(_ path: String) {
        
    }
}


extension TraktManager.TraktError: Equatable {
    public static func == (lhs: TraktManager.TraktError, rhs: TraktManager.TraktError) -> Bool {
            return true // FIX
    }
}
