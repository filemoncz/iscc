//
//  VLCMediaPlayer+Aspect+.h
//  StreamCinema.atv
//
//  Created by iSCC on 01/10/2021.
//  Copyright © 2021 SCC. All rights reserved.
//


#ifndef VLCMediaPlayer__h
#define VLCMediaPlayer__h
#include <Availability.h>
#include <TargetConditionals.h>

#if TARGET_OS_IOS
#import <MobileVLCKit/MobileVLCKit.h>
#else
#import <TVVLCKit/TVVLCKit.h>
#endif

#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>

@interface AVAudioSession()
-(id _Nullable)audioFormats;
-(long long)audioFormat;
-(BOOL)isDolbyAtmosAvailable;
-(BOOL)isDolbyDigitalEncoderAvailable;
-(BOOL)setAudioHardwareControlFlags:(unsigned long long)arg1 error:(id _Nonnull)arg2 ;
-(BOOL)isHardwareFormatFixedToMultiChannel;
-(BOOL)fixHardwareFormatToMultiChannel:(BOOL)arg1 error:(id _Nonnull)arg2 ;
-(void)privateUpdateAudioFormat:(id _Nonnull)arg1 ;
-(void)privateUpdateAudioFormats:(id _Nonnull)arg1 ;
-(BOOL)privateSetPropertyValue:(unsigned)arg1 withBool:(BOOL)arg2 error:(id _Nonnull)arg3 ;
-(double)currentHardwareSampleRate;
-(long long)currentHardwareInputNumberOfChannels;
-(long long)currentHardwareOutputNumberOfChannels;
-(double)preferredHardwareSampleRate;
@end

@interface VLCMediaPlayer (Aspect)
-(void)setAspectRatio:(NSString* __nullable)aspectRatio;
-(NSString* __nullable)getAspectRatio;
@end

#endif /* VLCMediaPlayer__h */
