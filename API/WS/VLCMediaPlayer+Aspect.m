//
//  VLCMediaPlayer+VLCMediaPlayer_.h
//  StreamCinema.atv
//
//  Created by iSCC on 01/10/2021.
//  Copyright © 2021 SCC. All rights reserved.
//


#import "VLCMediaPlayer+Aspect.h"

@implementation VLCMediaPlayer (Aspect)

-(void)setAspectRatio:(NSString*)aspectRatio {
//    if (aspectRatio == nil) return;
//    self.videoCropGeometry = (char*)aspectRatio.UTF8String;
//    self.videoAspectRatio = (char*)aspectRatio.UTF8String;
//    self.scaleFactor = 0;
}

-(NSString*)getAspectRatio {
    if (self.videoCropGeometry != nil) {
        return [NSString stringWithUTF8String:self.videoCropGeometry];
    }
    return nil;
}

@end
