//
//  WSFile.swift
//  StreamCinema
//
//  Created by SCC on 26/05/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Moya
import XMLMapper
import Combine

public final class WSFile {

    let wsService: WSService

    init(wsService: WSService){
        self.wsService = wsService
    }

    func getFile(wsToken:String, ident:String, uuid:UUID, type:String = "video_stream") -> AnyPublisher<WSStreamModel, Error> {
        var data:[String:Any] = [:]
        data["device_uuid"] = uuid.uuidString
        data["ident"] = ident
        data["download_type"] = type
        data["wst"] = wsToken
        data["device_vendor"] = "iscc"
        return wsService.getStreamLink(params: data)
    }
    
    func getFileAsync(wsToken:String, ident:String, uuid:UUID, type:String = "video_stream") async throws -> WSStreamModel {
        var data:[String:Any] = [:]
        data["device_uuid"] = uuid.uuidString
        data["ident"] = ident
        data["download_type"] = type
        data["wst"] = wsToken
        data["device_vendor"] = "iscc"
        return try await wsService.getStreamLink(params: data)
    }
}
