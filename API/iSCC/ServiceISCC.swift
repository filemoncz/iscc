//
//  FindService.swift
//  StreamCinema
//
//  Created by SCC on 08/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import Moya
import Combine
import XMLMapper

final class ServiceISCC {
    let apiClient = Provider.isccRequest
    
    //MARK: - Sources Endpoind
    public func connect() -> AnyPublisher<Bool, Error> {
        apiClient.requestWithoutMapping(request: RequestISCC.postData)
            .map { _ -> Bool in return true }
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }
    
    public func getTVdata() -> AnyPublisher<TVs, Error>  {
        apiClient.requestArr(request: RequestISCC.getTvData, model: TVs.self)
    }

    
    public func getEPG(for station: String) -> AnyPublisher<StationEPG, Error> {
        apiClient.request(request: RequestISCC.getEPGData(station: station), model: StationEPG.self)
    }
    
    public func addShare(sccID: String, myHash: String, isMovie: Bool, isPositive: Bool) async throws -> FriendsShareModels {
        let target = RequestISCC.addRating(sccID: sccID, userName: myHash, isMovie: isMovie, isPositive: isPositive)
        return try await requestRatingsAsync(target, myHash: myHash)
    }
    
    public func removeShare(sccID: String, myHash: String, isMovie: Bool) async throws -> FriendsShareModels {
        let target = RequestISCC.removeRating(sccID: sccID, userName: myHash, isMovie: isMovie)
        return try await requestRatingsAsync(target, myHash: myHash)
    }
    
    public func allMovieRatings(sccID: String, myHash:String) async throws -> FriendsShareModels {
        let target = RequestISCC.allMovieRatings(sccID: sccID)
        return try await requestRatingsAsync(target, myHash: myHash)
    }

    public func asyncShared(from userIDs:[String], type: FilterType) async throws -> FriendsShareModels {
        let target = RequestISCC.allRatingsFrom(userName: userIDs)
        let data = try await requestAsync(target)
        let models = try JSONDecoder().decode(FriendsShareModels.self, from: data)
        if type == .movie {
            return models.filter({ !$0.isTVShow })
        }
        return models.filter({ $0.isTVShow })
    }
    
    private func requestRatingsAsync(_ target: RequestISCC, myHash:String) async throws -> FriendsShareModels{
        let data = try await requestAsync(target)
        var models = try JSONDecoder().decode(FriendsShareModels.self, from: data)
        for index in models.indices {
            models[index].isMe = models[index].name == myHash
        }
        return models
    }
    
    private func requestAsync(_ target: RequestISCC) async throws -> Data {
        return try await withCheckedThrowingContinuation({ continuation in
            apiClient.request(target) { result in
                switch result {
                case .success(let moyaResponse):
                    if moyaResponse.statusCode == 200 {
                        continuation.resume(returning: moyaResponse.data)
                    } else if let error = HTTPStatusCode(rawValue: moyaResponse.statusCode) {
                        continuation.resume(throwing: error)
                    } else {
                        continuation.resume(throwing: CommonErrors.unownedStatusCode)
                    }
                case .failure(let error):
                    continuation.resume(throwing: error)
                }
            }
        })
    }
}
