//
//  AppData.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TraktKit

final class AppData {
    public var isDisableAdult: Bool = CurrentAppSettings.isPassCodeEnabled
    
    public let scService: SC2Service = SC2Service()
    public let wsService: WSService = WSService()
    public let osService: OSService = OSService()
    public let isccService: ServiceISCC = ServiceISCC()
    public let gitLabService: ServiceGitLab = ServiceGitLab()
    
    public var traktSync: SyncTrakt?
    public var speed: SpeedTest
    var wsFileService: WSFile
    public var token: WSTokenData?
    public var osLogin: OSLoginResult?
    
    public var loginManager: LoginManager
    public let tabBarManager: TabBarManager
    var traktManager: TraktManager?

    init(traktManager: TraktManager) {
        if traktManager.isSignedIn {
            self.traktManager = traktManager
            self.traktSync = SyncTrakt()
        }
        let tabBarManager = TabBarManager(traktManager: traktManager)
        self.tabBarManager = tabBarManager
        let wsFile = WSFile(wsService: wsService)
        self.wsFileService = wsFile
        self.speed = SpeedTest(wsFileService: wsFile)
        self.loginManager = LoginManager(wsService: wsService, osService: osService)
        
//        Task {
//            var friends = FriendData.friends
//            let name = "LoginName"
//            guard let salt = try? await wsService.getSaltAsync(salt: name).salt else { return }
//            let hash = loginManager.sha256(name: name, salt: salt)
//            let newF = Friend(name: name, alias: "Alias", hasnedName: hash)
//            friends.append(newF)
//            FriendData.friends = friends
//        }
    }
}
