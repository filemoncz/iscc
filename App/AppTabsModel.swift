//
//  AppTabsModel.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation
import SwiftUI

enum TabBarItem: Int, CaseIterable {
    // NOTE: The numbers are equivalent with position on tabbar.
    // If the item is not on tab bar, just give hime some last numbers.
    case search = 0
    case movies
    case tvShows
    case concerts
    case trakt
    case fullTextSearch
    case studios
    case settings
    
    var description: String {
        switch self {
        case .movies: return String(localized: .movies)
        case .tvShows: return String(localized: .series)
        case .fullTextSearch: return String(localized: .search)
        case .search: return String(localized: .az_Search)
        case .settings: return String(localized: .settings)
        case .trakt: return String(localized: .trakt)
        case .studios: return String(localized: .studios)
        case .concerts: return String(localized: .concerts)
        }
    }

    var systemImage: String {
        switch self {
        case .movies: return "film"
        case .tvShows: return "play.tv"
        case .concerts: return "tv.music.note"
        case .search: return "magnifyingglass"
        case .trakt: return "list.and.film"
        case .fullTextSearch: return "magnifyingglass"
        case .studios: return "list.bullet.rectangle"
        case .settings: return "gearshape"
        }
    }

    var image: String {
        switch self {
        case .movies: return "film"
        case .tvShows: return "play.tv"
        case .concerts: return "tv.music.note"
        case .search: return "magnifyingglass"
        case .trakt: return "trakt"
        case .fullTextSearch: return "rectangle.and.text.magnifyingglass"
        case .studios: return "list.bullet.rectangle"
        case .settings: return "gearshape"
        }
    }
    
    var type: FilterType? {
        switch self {
        case .movies:
            return .movie
        case .tvShows:
            return .tvshow
        case .search:
            return .all
        case .trakt:
            return .trakt
        case .concerts:
            return .concert
        case .settings,
             .studios,
             .fullTextSearch:
            return nil
        }
    }

    var isLast: Bool {
        return self == .studios
    }

    var isFirst: Bool {
        return self == .fullTextSearch
    }


    static var defaultItems: [TabBarItem] {
        return  [.fullTextSearch, .movies, .tvShows, .concerts, .search, .studios]
    }

}
