//
//  SubtitlesSettings.swift
//  StreamCinema.atv
//
//  Created by Jakub Cizek on 21/08/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

enum SubtitleColor: String, CaseIterable {
    case white = "0x00FFFFFF"
    case yellow = "0x00FFFF00"
    case black = "0x00000000"
    case red = "0x00FF0000"
    
    var color: UIColor {
        switch self {
        case .white:
            return .white
        case .yellow:
            return .yellow
        case .black:
            return .black
        case .red:
            return .red
        }
    }
    var colorName:String {
        switch self {
        case .white:
            return "white"
        case .yellow:
            return "yellow"
        case .black:
            return "black"
        case .red:
            return "red"
        }
    }
}

enum SubtitleSize: Int {
    case small = 18
    case normal = 16
    case large = 12
    case larger = 6
    
    var description: String {
        switch self {
        case .small:
            return String(localized: .subtitles_font_size_small)
        case .normal:
            return String(localized: .subtitles_font_size_normal)
        case .large:
            return String(localized: .subtitles_font_size_large)
        case .larger:
            return String(localized: .subtitles_font_size_larger)
        }
    }
}

struct SubtitlesSettings {
    private static let SubtitlesSizeOptions = "--freetype-rel-fontsize="
    private static let SubtitlesSizeOptions1 = "--freetype-fontsize="
    private static let SubtitlesColorOptions = "--freetype-color="
    
    private init() { }
    
    static func getSubtitlesOptions() -> [String] {
        var options:[String] = []
        let savedSize = SubtitlesSettings.subtitlesSize
        options.append(SubtitlesSizeOptions + String(savedSize.rawValue))

//        options.append(SubtitlesSizeOptions1 + String(savedSize.rawValue))
        
        let savedColor = SubtitlesSettings.subtitlesColor
        options.append(SubtitlesColorOptions + savedColor.rawValue)
        
        return options
    }
    
    static var subtitlesSize: SubtitleSize {
        get {
            return SubtitleSize(rawValue: UserDefaults.standard.integer(forKey: SubtitlesSettings.SubtitlesSizeOptions)) ?? .large
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: SubtitlesSettings.SubtitlesSizeOptions)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var subtitlesColor: SubtitleColor {
        get {
            return SubtitleColor(rawValue: UserDefaults.standard.string(forKey: SubtitlesSettings.SubtitlesColorOptions) ?? "") ?? .white
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: SubtitlesSettings.SubtitlesColorOptions)
            UserDefaults.standard.synchronize()
        }
    }
}
