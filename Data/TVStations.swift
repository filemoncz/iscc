//
//  TVStations.swift
//  StreamCinema.atv
//
//  Created by SCC on 29/09/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

enum TVStations: String, CaseIterable {
    case All = "all"
    case AMC
    case AXN
    case AXN_Black = "AXN Black"
    case AXN_White = "AXN White"
    case Animal_Planet = "Animal Planet"
    case Barrandov
    case Barrandov_Krimi = "Barrandov Krimi"
    case Barrandov_News = "Barrandov News"
    case CBS_Reality = "CBS Reality"
    case CS_History = "CS History"
    case CS_Horror = "CS Horror"
    case CS_Mystery = "CS Mystery"
    case CSfilm
    case Cinemax
    case Cinemax2
    case Discovery_Channel = "Discovery Channel"
    case Discovery_HD_Showcase = "Discovery HD Showcase"
    case Discovery_Science = "Discovery Science"
    case Discovery_Turbo_Xtra = "Discovery Turbo Xtra"
    case Disney_Channel = "Disney_Channel"
    case E_Entertainment = "E! Entertainment"
    case Epic_Drama = "Epic Drama"
    case Film_Europe_plus = "Film Europe +"
    case Film_Europe_Channel = "Film Europe Channel"
    case Film_plus = "Film+"
    case FilmBox = "FilmBox"
    case FilmBox_Extra_HD = "FilmBox Extra HD"
    case FilmBox_Plus = "FilmBox Plus"
    case Filmbox_Family = "Filmbox Family"
    case Filmbox_Premium = "Filmbox Premium"
    case HBO = "HBO"
    case HBO2 = "HBO2"
    case HBO3 = "HBO3"
    case History_Channel = "History Channel"
    case Investigation_Discovery = "Investigation Discovery"
    case JOJ_Cinema = "JOJ Cinema"
    case JOJ_Family = "JOJ Family"
    case JimJam = "JimJam"
    case Kino_Barrandov = "Kino Barrandov"
    case MTV_CZ = "MTV CZ"
    case Minimax = "Minimax"
    case National_Geographic = "National Geographic"
    case National_Geographic_Wild = "National Geographic Wild"
    case Nickelodeon_Commercial = "Nickelodeon Commercial"
    case Nickelodeon_Global = "Nickelodeon Global"
    case Nova = "Nova"
    case Nova_2 = "Nova 2"
    case Nova_Action = "Nova Action"
    case Nova_Cinema = "Nova Cinema"
    case Nova_Gold = "Nova Gold"
    case Nova_International = "Nova International"
    case Prima = "Prima"
    case Prima_Comedy_Central = "Prima Comedy Central"
    case Prima_Cool = "Prima Cool"
    case Prima_Krimi = "Prima Krimi"
    case Prima_Love = "Prima Love"
    case Prima_MAX = "Prima MAX"
    case Prima_Plus = "Prima Plus"
    case Prima_ZOOM = "Prima ZOOM"
    case Seznam_cz = "Seznam.cz"
    case Spektrum = "Spektrum"
    case Spektrum_Home = "Spektrum Home"
    case TLC = "TLC"
    case Travel_Channel = "Travel Channel"
    case Viasat_Explorer = "Viasat Explorer"
    case Viasat_History = "Viasat History"
    case Viasat_Nature = "Viasat Nature"
    case CTD = "ČT:D"
    case CT_art = "ČT art"
    case CT1 = "ČT1"
    case CT2 = "ČT2"
    case CT3 = "ČT3"
    case Dajto = "Dajto"
    case Doma = "Doma"
    case Dvojka = "Dvojka"
    case JOJ = "JOJ"
    case Jednotka = "Jednotka"
    case Jojko = "Jojko"
    case Markiza = "Markíza"
    case Markiza_International = "Markíza International"
    case Plus = "Plus"
    case Trojka = "Trojka"
    case Wau = "Wau"
    
    var imageURL: URL? {
        return URL(string: "")
    }
}
