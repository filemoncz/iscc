# SCC for Apple

- verzia len pre Apple TV - tvOS 13 a novsie
- aplikacia pouziva externe kniznice ktore su obsahom balika
- aplikacia sa pripaja na servre scc, webshare neskor trakt.tv a opensubtitles.org

# Ako instalovat

    Existuju 2 moznosti:

        - 1 z ipa suboru za predpokladu ze mate kupeny certifikat

            - stiahnut subor "scc.ipa"

            - nainstalovat https://dantheman827.github.io/ios-app-signer/

            - prepodpisat ipu

            - pomocou xcode nahrat ipu do AppleTV

        - 2 stiahnut cely projekt (ak nemate plateny dev ucet ale len free dev ucet aplikacia bude fungovat len 7 dni)

            - otovirt projekt cez ...xcworkspace

            - vybrat vasu apple tv

            - buildnut aplikaciu

# Ako buildovat

    Ak chcete prepodpisat ipu a nahrata ipa je stara mozete si urobit vlastnu ipu

        - stiahnut cely projekt

        - spustit terminal

        - pomocou prikazu CD sa musite dostat do root adresaru projektu
        
        - nainstalovat cocoapods -> https://guides.cocoapods.org/using/getting-started.html
        
        - zadat prikazd `pod install`, nainstaluje sa VLCKit
        
        - otovrit projekt, pockat kym sa dotahaju vsetky dependencie

        - nasledovnym prikazom sa spusti build `xcodebuild -workspace StreamCinema.atv.xcworkspace -scheme StreamCinema.atv -configuration Release clean archive -archivePath buildArchive/StreamCinema.atv.xcarchive CODE_SIGN_IDENTITY='' CODE_SIGNING_REQUIRED=NO CODE_SIGNING_ALLOWED=NO`

        - nasledne zobrazit content cez Finder xarchivu .../buildArchive/StreamCinema.atv.xcarchive/Products/Applications

        - na ploche vytvorit adresar Payload do neho nakopirovat scc.app
        
        - zazipovat payload adresar

        - premenvat Payload.zip na scc.ipa
 


# Licencia
    
    Ziaden z autorov tejto aplikacie nieje vlastnikom akehokolvek obsahu ktory je zobrazovany v aplikacii. 
    Tato aplikacia sluzi len na studine ucely. 
    Tato aplikacia nema nic spolocne so zdrojom dat a udajov.
    Je to len aplikacia tretej strany a nikto z tvorcov nezodpoveda za jej obsah alebo pripadne pouzite.
