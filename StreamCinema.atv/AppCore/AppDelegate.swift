//
//  AppDelegate.swift
//  StreamCinema.atv
//
//  Created by SCC on 23/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import CoreData
import os.log
import TraktKit
//import Bugsnag
import FanArtKit
import TMDBKit
import Combine

extension Notification.Name {
    static let TraktSignedIn = Notification.Name(rawValue: "TraktSignedIn")
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    public let appData = AppData(traktManager: TraktManager.sharedManager)
    public var loginManager: LoginManager?
    private(set) var appCoordinator: AppCoordinator!
    private var cancelables: Set<AnyCancellable> = Set()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

//        Bugsnag.start()

        TraktManager.sharedManager.set(clientID: Constants.clientId,
                                       clientSecret: Constants.clientSecret,
                                       redirectURI: Constants.redirectURI)
        FanartClient.shared.apiKey = Constants.fanArtApiKey
        TMDBConfig.apikey = Constants.tmdbApiKey


        appCoordinator = AppCoordinator(window: window!, appData: appData)
        appCoordinator.start()


        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func syncTrakt() {
        DispatchQueue.main.async {
            self.appData.traktSync?.start(completition: nil)
        }
    }
    
    func refreshTraktToken() {
        TraktManager.sharedManager.checkToRefresh { result in
            switch result {
            case .success():
                Log.write("TraktManager checkToRefresh success")
                self.syncTrakt()
            case .failure(let error):
                Log.write("TraktManager checkToRefresh failure \(error)")
            }
            
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        Log.clearOldLogs()
        if TraktManager.sharedManager.isSignedIn {
            switch TraktManager.sharedManager.refreshState {
            case .noTokens:
                break
            case .validTokens:
                self.syncTrakt()
            case .refreshTokens,
                 .expiredTokens:
                self.refreshTraktToken()
            }
        }
        appData
            .isccService
            .connect()
            .sink { completion in
                guard case .failure(let err) = completion else { return }
                Log.write("applicationDidBecomeActive isccService failure. Reason: \(err.localizedDescription)")
            } receiveValue: { (_) in
                Log.write("applicationDidBecomeActive isccService success")
            }.store(in: &cancelables)


        appData
            .loginManager
            .connectWithExistingData()
            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
            .store(in: &cancelables)

        appData
            .gitLabService
            .getReleases()
            .sink { completion in
                guard case .failure(let err) = completion else { return }
                Log.write("applicationDidBecomeActive getReleses failed: \(err.localizedDescription)")
            } receiveValue: { [weak self] release in
                Log.write("Last released: \(release)")
                self?.chceck(release.name, desc: release.welcomeDescription)
            }.store(in: &cancelables)

    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        Log.write("Will open URL from Top Shelf. URL: \(url)")
        
        if let params = url.queryParameters,
           let startPlayingString = params["startPlaying"],
           let startPlaying = startPlayingString.bool,
           let typeString = params["type"],
           let type = FilterType(rawValue: typeString),
           let movieID = params["movieID"] {

            appCoordinator.presentDetailFormOpenIn(type: type, movieID: movieID, startPlaying: startPlaying)
        }
        return false
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "WatchedList")
        
        //        if let storeURL = self.storeUrl {
        //            container.persistentStoreDescriptions.removeAll()
        //            container.persistentStoreDescriptions = [NSPersistentStoreDescription(url: storeURL)]
        //        }
        //file:///var/mobile/Containers/Data/Application/298A0DBC-2F99-4FA0-ACEB-D317C8882C9A/Library/Caches/WatchedList.sqlite
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Unresolved error, \((error as NSError).userInfo)")
            }
        })
        return container
    }()
    
    private func migrate(from oldUrl:URL?, to newUrl:URL) {
        guard let oldUrl = oldUrl else { return }
        if FileManager.default.fileExists(atPath: oldUrl.path),
           !FileManager.default.fileExists(atPath: newUrl.path) {
            do {
                
                try FileManager.default.moveItem(at: oldUrl, to: newUrl)
            } catch let err {
                Log.write("\(err)")
            }
        }
    }
    
    private var storeUrl: URL? {
        get {
            let urls = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
            if let url = urls.first {
                return url.appendingPathComponent("WatchedList.sqlite")
            }
            return nil
        }
    }
}

extension AppDelegate {

    private struct Constants {
        static let clientId = "0dc11b34ed91a94fb03ecf3cd2b7d659d2ec1c22e9d78592da76f136320b40ce"
        static let clientSecret = "6e8bb0e23b73f15220c1fcc62b13a00c9bdcba663fe8247632e888d19c387f4c"
        static let redirectURI = "scctrakt://auth/trakt"
        static let tmdbApiKey = "4efc6b294047496e70cc140ee784bd0f"
        static let fanArtApiKey = "aae397341f665560e6cc0f8ec7880d1b"
    }
    
    private func chceck(_ version:String, desc: String) {
        guard let vc = self.window?.rootViewController,
              let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else { return }

        if !version.contains(appVersion) {
            var message = String(format: String(localized: .new_version_messsage), appVersion,version)
            message = message + "\n" + desc.replacingOccurrences(of: "\n\n", with: "\n")
            if MobileProvision.read()?.entitlements.identifier == "5PUJGSG8GM" {
                message = message + "\n https://iscc.site/"
            }
            let alert = UIAlertController(title: String(localized: .new_version), message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: String(localized: .button_continue), style: .cancel, handler: nil))

            vc.present(alert, animated: true, completion: nil)
        }
    }
}

extension URL {
    public var queryParameters: [String: String]? {
        guard
            let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else { return nil }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
}

extension Notification.Name {
    static let focusTabBar = Notification.Name("force-focus-tabbar")
}

extension String {
    var bool: Bool? {
        switch self.lowercased() {
        case "true", "t", "yes", "y", "1":
            return true
        case "false", "f", "no", "n", "0":
            return false
        default:
            return nil
        }
    }
}
