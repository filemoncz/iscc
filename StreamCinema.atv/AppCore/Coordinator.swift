//
//  Coordinator.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 21.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit


///
/// PRESENTING CHILD COORDINATOR
///
/// 1. Initializing coordinator
/// let launchCoordinator = LaunchCoordinator()
/// launchCoordinator.onFinish = { [weak self] in
///   self?.startLogin()
/// }
///
/// 2. Adding as child
/// addChildCoordinator(launchCoordinator)
///
/// 3. Start
/// launchCoordinator.start()
///
/// 4. Placing it's view controller in hierarhy
/// window.rootViewController = launchCoordinator.rootViewController
///
/// FINISHING THE FLOW
///
/// When coordinator finishes it's flow, call coordinator.finish()
/// This method will deattach coordinator from it's parent, finish
/// all child coordinators if has and call onFinish callback.
///
/// If coordinator's view controller is embedded in shared navigation controller
/// and should finish it's flow when back button is tapped, then implement
/// BackButtonCatching in the view controller and override (or append)
/// willMoveToParent method:
///
/// override func didMove(toParentViewController parent: UIViewController?) {
///   super.didMove(toParentViewController: parent)
///   testBackButtonMoving(toParentViewController: parent)
/// }
///
/// In your coordinator, when initializing view controller, call
/// finishOnBackButton(ofViewController: yourViewController)
///

//public protocol Coordinator: AnyObject {
//  var onFinish: (() -> Void)? { get set }
//
//  var childCoordinators: [Coordinator] { get set }
//  /// This should be weak, but it is not declared in the protocol as declaring weak in the protocol will not be supported in future xcode versions
//  var parentCoordinator: Coordinator? { get set }
//
//  /// Start coordinator's flow
//  func start()
//
//  /// Finishes the flow, call's onFinish callback and removes from parent coordinator
//  func finish()
//
//  /// Override this method if coordinator should do smth before finishing
//  func willFinish()
//
//  /// Adds given coordinator to array of child coordinators
//  func addChildCoordinator(_ coordinator: Coordinator)
//}
//
//public extension Coordinator {
//  func addChildCoordinator(_ coordinator: Coordinator) {
//    childCoordinators.append(coordinator)
//    coordinator.parentCoordinator = self
//  }
//}


//open class BaseCoordinator: NSObject, Coordinator {
//  public var onFinish: (() -> Void)?
//
//  public var childCoordinators: [Coordinator] = []
//  public weak var parentCoordinator: Coordinator?
//
//  private var isFinished = false
//  private let debug = false
//
//  deinit {
//    finish()
//  }
//
//  /// Starts coordinator's flow
//  open func start() {
//    if debug {
//      print("===== START: \(self) =====")
//    }
//  }
//
//  /// Finishes the coordinator's flow
//  public func finish() {
//    guard !isFinished else {
//      return
//    }
//    willFinish()
//    childCoordinators.forEach { $0.finish() }
//    onFinish?()
//    onFinish = nil
//    removeFromParentCoordinator()
//    isFinished = true
//    if debug {
//      print("====== FINISH: \(self) ======")
//    }
//  }
//
//  public func willFinish() {}
//
//  private func removeFromParentCoordinator() {
//    guard let idx = parentCoordinator?.childCoordinators.firstIndex(where: { $0 === self }) else {
//      return
//    }
//    parentCoordinator?.childCoordinators.remove(at: idx)
//    parentCoordinator = nil
//  }
//}
