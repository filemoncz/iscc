//
//  AZSearchController.swift
//  StreamCinema.atv
//
//  Created by SCC on 01/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine

enum KeyboardFunc: Int {
    case space = 909
    case backpace = 999
}

struct AZModel {
    var char: String
}

final class AZSearchController: UIViewController {

    static let vcIdentifier = "AZSearchController"
    static func create(appData: AppData) -> AZSearchController {
        let storyboard = UIStoryboard.main
        let vc = storyboard.instantiateViewController(withIdentifier: vcIdentifier) as! AZSearchController
        vc.appData = appData
        return vc
    }

    @IBOutlet var resultCollection: MovieCollectionView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var button0: UIButton!
    @IBOutlet var buttonI: UIButton!
    @IBOutlet var typeSegment: UISegmentedControl!
    
    @IBOutlet var keyboardButtons: [UIButton]!
    @IBOutlet var numberKeyboardButtons: [UIButton]!
    
    private let midleDownKeyboard:UIFocusGuide = UIFocusGuide()
    private let midleUPKeyboard:UIFocusGuide = UIFocusGuide()
    private var filterType:FilterType = .movie
    private var coordinator: MovieCoordinator?

    var onPresentMovieDetailScreen: ((SCCMovie) -> Void)?

    private var appData: AppData!
    private let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()
    
    var model: SCCMovieResult? {
        willSet {
            if self.resultCollection != nil {
                self.resultCollection.model = newValue
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCollectionView()
        self.setupFocusGuides()
        setupSubsriptions()
        setupMenuButtonToForceFocusOnTabBar()
        
        self.typeSegment.setTitle(String(localized: .movies2), forSegmentAt: 0)
        self.typeSegment.setTitle(String(localized: .series), forSegmentAt: 1)
    }

    private func setupSubsriptions() {
        errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] (error) in
                guard let otherError = self?.handleCommonError(error) else { return }
                self?.presentErrorAlert(error: otherError)
            }.store(in: &cancelables)
    }
    
    @IBAction func typeSegmentDidChange(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.filterType = .movie
        } else {
            self.filterType = .tvshow
        }
        if let text = self.searchTextField.text {
            self.updateSearch(text: text, startIndex: 0)
        }
    }
    
    
    private func setupFocusGuides() {
        self.view.addLayoutGuide(self.midleUPKeyboard)
        self.view.addLayoutGuide(self.midleDownKeyboard)
        
        self.midleDownKeyboard.topAnchor.constraint(equalTo: self.buttonI.bottomAnchor, constant: 1).isActive = true
        self.midleDownKeyboard.bottomAnchor.constraint(equalTo: self.midleUPKeyboard.topAnchor, constant: 0).isActive = true
        self.midleDownKeyboard.heightAnchor.constraint(equalToConstant: 4).isActive = true
        self.midleDownKeyboard.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.midleDownKeyboard.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        
        self.midleUPKeyboard.bottomAnchor.constraint(equalTo: self.button0.topAnchor, constant: -1).isActive = true
        self.midleUPKeyboard.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        self.midleUPKeyboard.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        
        self.updatefocus()
    }
    
    private func updatefocus() {
        self.midleUPKeyboard.preferredFocusEnvironments = self.keyboardButtons
        
        let focusEnvi = self.numberKeyboardButtons.sorted(by: {
            if let title1 = $0.currentTitle, let title2 = $1.currentTitle {
                return title1 < title2
            }
            return false
        }).filter( { $0.isEnabled == true } )
        
        if focusEnvi.count != 0 {
            self.midleDownKeyboard.preferredFocusEnvironments = focusEnvi
        } else {
            self.midleDownKeyboard.preferredFocusEnvironments = [self.resultCollection]
        }
    }
    
    private func setupCollectionView() {
        self.resultCollection.movieDelegate = self
        self.resultCollection.register(UINib(nibName: "MovieCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
    }
    
    @IBAction func keyboardButtonPressed(_ sender: UIButton) {
        if sender.tag == KeyboardFunc.backpace.rawValue, var text = self.searchTextField.text, !text.isEmpty,
            let lastCh = text.last, let index = text.firstIndex(of: lastCh) {
            text.remove(at: index)
            self.updateSearch(text: text, startIndex: 0)
        } else if sender.tag == KeyboardFunc.space.rawValue,
            var text = self.searchTextField.text {
            text.append(" ")
            self.updateSearch(text: text, startIndex: 0)
        } else if sender.tag != KeyboardFunc.space.rawValue,
            sender.tag != KeyboardFunc.backpace.rawValue,
            var text = self.searchTextField.text {
            text += (sender.titleLabel?.text)!
            self.updateSearch(text: text, startIndex: 0)
        }
    }
    
    private func updadateKeyboard(with results: [AZResult]) {
        self.allButtons(isEnabled: false)
        for result in results {
            if let searchedText = self.searchTextField.text, //AA
                let value = result.value { //AAA
                let char = value.replacingOccurrences(of: searchedText, with: "")
                self.enableButton(title: char)
            }
        }
        self.updatefocus()
    }
    
    private func enableButton(title: String) {
        for button in self.keyboardButtons {
            if button.titleLabel?.text == title {
                button.isEnabled = true
                return
            }
        }
        for button in self.numberKeyboardButtons {
            if button.titleLabel?.text == title {
                button.isEnabled = true
                return
            }
        }
    }
    
    private func allButtons(isEnabled: Bool) {
        for button in self.keyboardButtons {
            button.isEnabled = isEnabled
        }
        for button in self.numberKeyboardButtons {
            button.isEnabled = isEnabled
        }
    }
    
    private func updateSearch(text: String, startIndex: Int ) {
        self.searchTextField.text = text
        self.model = nil
        if self.searchTextField.text?.isEmpty ?? false {
            self.allButtons(isEnabled: true)
            return
        }
        self.activityIndicator.startAnimating()

        let filter = FilterModel.azInit(type: self.filterType, value: text, startIndex: startIndex)

        appData.scService
            .azSearch(model: filter)
            .assignError(to: errorSubject)
            .sink { [weak self] azResult in
                if let data = azResult.data {
                    self?.updadateKeyboard(with: data)
                }
            }.store(in: &cancelables)

        let find = FilterModel.searchStartSimpleInit(type: self.filterType, value: text, startIndex: startIndex)

        appData.scService
            .search(wtih: find)
            .assignError(to: errorSubject)
            .handleEvents(receiveCompletion: { [weak self] _ in
                self?.activityIndicator.stopAnimating()
            })
            .sink { [weak self] azResult in
                self?.model = azResult
            }.store(in: &cancelables)

    }
}

extension AZSearchController: MovieCollectionDelegate {
    func movieCollection(_ collection: MovieCollectionView, startIndex: Int) {
        
    }
    
    func movieCollection(_ collection: MovieCollectionView, didSelect movie: SCCMovie) {
        onPresentMovieDetailScreen?(movie)
    }
}

