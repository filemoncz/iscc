//
//  CellType.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation
import UIKit

enum SettingsCellTypes {
    case userName
    case password
    case vipExpiration
    case resetWatchedhistory
    case resetButton
    case wsLogout
    case changeButton
    case wsSpeedTestResult
    case wsSpeedTestButton
    case loginTraktButton
    case traktUserName
    case traktUserIsVIP
    case traktLogout
    case openSubtitlesUserName
    case loginOpenSubtitles
    case subtitlesFontSize
    case subtitlesFontColor
    case disableErotic
    case appVersion
    case addFriend
    case deleteFiends
    case friendsList

    var cellTitle:String {
        get {
            switch self {
            case .userName,
                 .traktUserName,
                 .openSubtitlesUserName:
                return String(localized: .username)
            case .password:
                return String(localized: .password)
            case .vipExpiration:
                return String(localized: .remaining_days_of_provider_subs)
            case .changeButton:
                return String(localized: .button_changeCredentials)
            case .resetButton:
                return String(localized: .buttonReset)
            case .wsSpeedTestButton:
                return String(localized: .buttonStartSpeed)
            case .wsSpeedTestResult:
                return String(localized: .last_speedTest_resutl)
            case .resetWatchedhistory:
                return String(localized: .button_resetWatched)
            case .loginTraktButton:
                return String(localized: .button_changeCredentials)
            case .wsLogout :
                return String(localized: .logoutButton)
            case .traktUserIsVIP:
                return String(localized: .isVIP)
            case .loginOpenSubtitles:
                return String(localized: .loginButton)
            case .subtitlesFontSize:
                return String(localized: .subtitles_font_size)
            case .subtitlesFontColor:
                return String(localized: .subtitles_font_color)
            case .disableErotic:
                return String(localized: .disableErotic)
            case .appVersion:
                return String(localized: .appVersion)
            case .traktLogout:
                return String(localized: .logoutButton)
            case .addFriend:
                return String(localized: .firends_add)
            case .deleteFiends:
                return String(localized: .firends_remove)
            case .friendsList:
                return String(localized: .firends_list)
            }
        }
    }
    
    var desc: String {
//        switch self {
//        case .userName, .password,
//                .vipExpiration, .resetWatchedhistory,
//                .wsSpeedTestResult, .traktUserName,
//                .traktUserIsVIP, .appVersion:
//            return cellTitle
//        case .resetButton:
//            <#code#>
//        case .wsLogout:
//            <#code#>
//        case .changeButton:
//            <#code#>
//        case .wsSpeedTestButton:
//            <#code#>
//        case .loginTraktButton:
//            <#code#>
//        case .openSubtitlesUserName:
//            <#code#>
//        case .loginOpenSubtitles:
//            <#code#>
//        case .subtitlesFontSize:
//            <#code#>
//        case .subtitlesFontColor:
//            <#code#>
//        case .disableErotic:
//            <#code#>
//        }
        return "Desc"
    }
    
    var image: UIImage? {
        if self == .loginTraktButton {
            return UIImage(named: "traktLogo")
        }
        if self == .wsLogout || self == .wsSpeedTestResult || self == .wsSpeedTestButton {
            return UIImage(named: "webshareLogo")
        }
        return UIImage(named: "App Icon")
    }
}
