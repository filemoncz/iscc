//
//  SettingMenuItem.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation


enum SettingsMenuItem: CaseIterable {
    case webshare
    case trakt
    case openSubtitles
    case subtitles
    case friends

    var description: String {
        switch self {
        case .webshare:
            return "Webshare"
        case .trakt:
            return "Trakt.tv"
        case .openSubtitles:
            return "OpenSubtitles"
        case .subtitles:
            return String(localized: .subtitles_menu_item)
        case .friends:
            return "Fiedns"
        }
    }

    var sections: [SettingsSections] {
        return SettingsSections.sections(for: self)
    }

}
