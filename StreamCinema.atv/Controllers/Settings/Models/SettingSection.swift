//
//  SettingSection.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation
import TraktKit


// MARK: - Nested data types
 enum SettingsSections: Int {
     case wsCredentials
     case wsInfo
     case other
     case traktCredentials
     case openSubtitlesCredentials
     case subtitlesFontSettings
     case friends

     var headerTitle:String {
         get {
             switch self {
             case .wsCredentials:
                 return String(localized: .provider_settings)
             case .wsInfo:
                 return String(localized: .provider_details)
             case .other:
                 return String(localized: .setting_other_app)
             case .traktCredentials:
                 return String(localized: .trakt_credentials)
             case .openSubtitlesCredentials:
                 return String(localized: .opensubtitles_credentials)
             case .subtitlesFontSettings:
                 return String(localized: .subtitles_font_setting)
             case .friends:
                 return String(localized: .friendsAdvise)
             }
         }
     }


    static func sections(for item: SettingsMenuItem) -> [SettingsSections] {
        switch item {
        case .webshare: return [.wsCredentials, .wsInfo, .other]
        case .trakt: return [.traktCredentials]
        case .openSubtitles: return [.openSubtitlesCredentials]
        case .subtitles: return [.subtitlesFontSettings]
        case .friends: return [.friends]
        }
    }

    var cells: [SettingsCellTypes] {
        switch self {
        case .wsCredentials: return [.userName,.password,.wsLogout]
        case .wsInfo: return [.vipExpiration]
        case .other: return [.wsSpeedTestResult,.wsSpeedTestButton,.resetWatchedhistory,.resetButton, .disableErotic, .appVersion]
        case .traktCredentials:
            if TraktManager.sharedManager.isSignedIn {
                return [.traktUserName,.traktUserIsVIP,.loginTraktButton,.traktLogout]
            } else {
                return [.loginTraktButton]
            }
        case .openSubtitlesCredentials:
            return [.openSubtitlesUserName,.loginOpenSubtitles]
        case .subtitlesFontSettings:
            return [.subtitlesFontSize,.subtitlesFontColor]
        case .friends:
            return [.friendsList, .addFriend,.deleteFiends]
        }
    }

 }
