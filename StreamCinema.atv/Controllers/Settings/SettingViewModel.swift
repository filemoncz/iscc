//
//  SettingViewModel.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import Combine

final class SettingsViewModel {

    private let menu: [SettingsMenuItem] = SettingsMenuItem.allCases
    var wsUserModel: UserModel?
    var traktUser: TraktUser?

    var errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()
    private let appData: AppData
    var onReloadData: (() -> Void)?

    init(appData: AppData) {
        self.appData = appData
    }

    //MARK: - Settings Menu Table
    func getNumberOfMenuItems() -> Int {
        self.menu.count
    }

    func getMenuTitle(at indexPath: IndexPath) -> String {
        return self.menu[indexPath.row].description
    }
    //MARK: - Settings Table
    func getNumberOfSections(for menu: SettingsMenuItem) -> Int {
        return menu.sections.count
    }

    func getSectionTitle(for section: SettingsSections) -> String {
        return section.headerTitle
    }

    func getNumberOfCells(for section: SettingsSections) -> Int {
        return section.cells.count
    }
    
    func isFocusableCell(_ tableView: UITableView, of section:SettingsSections, forRowAt indexPath:IndexPath) -> Bool {
        if let cellType = section.cells[safe: indexPath.row] {
            switch cellType {
            case .userName,
                 .password,
                 .vipExpiration,
                 .wsSpeedTestResult,
                 .traktUserName,
                 .traktUserIsVIP,
                 .appVersion,
                 .openSubtitlesUserName:
                return false
            case .resetWatchedhistory,
                 .resetButton,
                 .wsLogout,
                 .changeButton,
                 .wsSpeedTestButton,
                 .loginTraktButton,
                 .traktLogout,
                 .loginOpenSubtitles,
                 .subtitlesFontSize,
                 .subtitlesFontColor,
                 .disableErotic, .addFriend, .deleteFiends, .friendsList:
                return true
            }
        }
        return false
    }


    func getCell(_ tableView: UITableView, of section:SettingsSections, forRowAt indexPath:IndexPath) -> UITableViewCell? {
        let cells = section.cells
        let cellType = cells[indexPath.row]
        switch cellType {
        case .appVersion:
            let cell = InfoCell.getCell(tableView)
            cell.type = cellType
            if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
               let buildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                cell.set(text: String(localized: .appVersion) + " " + appVersion + "_" + buildNumber)
            }
            return cell
        case .password:
            let cell = SettingsInputTextCell.getCell(tableView)
            cell.type = cellType
            cell.set(name: cellType.cellTitle)
            if let pass = LoginManager.pass {
                cell.set(value: randomString(length: pass.count))
            } else {
                cell.set(value: "")
            }
            return cell
        case .userName:
            let cell = SettingsInputTextCell.getCell(tableView)
            cell.type = cellType
            cell.set(name: cellType.cellTitle)
            cell.set(value: LoginManager.name ?? "")
            return cell
        case .vipExpiration:
            let cell = InfoCell.getCell(tableView)
            cell.type = cellType
            if let days = wsUserModel?.vip_days {
                cell.set(text: String(localized: .remaining_days_of_provider_subs) + " " + days)
            }
            return cell
        case .resetButton,
             .wsSpeedTestButton,
             .changeButton,
             .resetWatchedhistory,
             .loginTraktButton,
             .loginOpenSubtitles,
             .wsLogout:
            let cell = ButtonCell.getCell(tableView)
            cell.type = cellType
            cell.set(text: cellType.cellTitle)
            return cell
        case .wsSpeedTestResult:
            let cell = InfoCell.getCell(tableView)
            cell.type = cellType
            cell.set(text: cellType.cellTitle + " \(appData.speed.lastSpeedString)")
            return cell
        case .traktUserName:
            let cell = SettingsInputTextCell.getCell(tableView)
            cell.type = cellType
            cell.set(name: cellType.cellTitle)
            cell.set(value: self.traktUser?.username ?? "")
            return cell
        case .traktUserIsVIP:
            let cell = SettingsInputTextCell.getCell(tableView)
            cell.type = cellType
            cell.set(name: cellType.cellTitle)
            cell.set(value: (self.traktUser?.isVIP ?? false) ? String(localized: .TRUE) : String(localized: .FALSE))
            return cell
        case .openSubtitlesUserName:
            let cell = SettingsInputTextCell.getCell(tableView)
            cell.type = cellType
            cell.set(name: cellType.cellTitle)
            cell.set(value: LoginManager.osName ?? "")
            return cell
        case .subtitlesFontSize:
            let cell = InfoCell.getCell(tableView)
            cell.type = cellType
            cell.set(text: String(localized: .subtitles_font_size) + ": " + SubtitlesSettings.subtitlesSize.description)
            return cell
        case .subtitlesFontColor:
            let cell = InfoCell.getCell(tableView)
            cell.type = cellType
            cell.set(text: String(localized: .subtitles_font_color) + ": " + SubtitlesSettings.subtitlesColor.colorName)
            return cell
        case .disableErotic:
            let cell = ButtonCell.getCell(tableView)
            cell.type = cellType
            if CurrentAppSettings.isPassCodeEnabled {
                cell.set(text: String(localized: .disableErotic) + ": " + String(localized: .enabled))
            } else {
                cell.set(text: String(localized: .disableErotic) + ": " + String(localized: .disabled))
            }
            return cell
        case .traktLogout:
            let cell = ButtonCell.getCell(tableView)
            cell.type = cellType
            cell.set(text: String(localized: .logoutButton))
            return cell
        case .addFriend, .deleteFiends, .friendsList:
            let cell = ButtonCell.getCell(tableView)
            cell.type = cellType
            cell.set(text: cellType.cellTitle )
            return cell
        }
    }

    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }

    func fetchTraktData() {
        appData.traktManager?
            .getUserProfile { [weak self] (result) in
                switch result {
                case .success(object: let object):
                    self?.traktUser = TraktUser(user: object)
                    self?.onReloadData?()
                case .error(error: let error):
                    guard let err = error else { return }
                    self?.errorSubject.send(err)
                }
            }
    }

    func fetchWSUserData() {
        guard let token = LoginManager.tokenHash,
        let uuid = UIDevice.current.identifierForVendor else { return }
        var data:[String:Any] = [:]
            data["device_uuid"] = uuid.uuidString
            data["wst"] = token

        appData.wsService
            .getUserData(params: data)
            .assignError(to: errorSubject)
            .sink { [weak self] (model) in
                self?.wsUserModel = model
                self?.onReloadData?()
            }.store(in: &cancelables)
    }

    func chceckCredentials(credentials: SettingsViewController.Credentials) {
        guard !credentials.name.isEmpty && !credentials.pass.isEmpty else { return }
        appData
            .loginManager
            .connect(name: credentials.name, pass: credentials.pass)
            .assignError(to: errorSubject)
            .sink { [weak self] _ in
                self?.fetchWSUserData()
            }.store(in: &cancelables)
    }

    func logout() -> AnyPublisher<Void, Never> {
        appData
            .loginManager
            .logout(isEraseAllData: false)
            .assignError(to: errorSubject)
            .map { _ in () }
            .eraseToAnyPublisher()
    }

    func connect(name: String?, pass: String?) -> AnyPublisher<Void, Error> {
        return appData.loginManager
            .connect(name: name, pass: pass)
            .map { _ in () }
            .eraseToAnyPublisher()
    }

    func testSpeed() {
        appData.speed
            .testSpeed()
            .assignError(to: errorSubject)
            .sink { [weak self] (bitRange, speedResult) in
                self?.onReloadData?()
            }.store(in: &cancelables)
    }

    func loginToOS(name: String?, pass: String?) {
        appData.loginManager
            .osLogin(name: name, pass: pass)
            .assignError(to: errorSubject)
            .map { _ in () }
            .sink { [weak self] _ in
                self?.onReloadData?()
            }.store(in: &cancelables)
    }


    func addFriend(for userName: String, alias: String, position: Int?) {
        Task {
            if let salt = try? await appData.wsService.getSaltAsync(salt: userName).salt {
                let hash = appData.loginManager.sha256(name: userName, salt: salt)
                
                var friends = FriendData.friends
                
                if let existIndex = friends.firstIndex(where: { $0.name == userName }) {
                    friends.remove(at: existIndex)
                }
                
                var sortIndex = position ?? friends.count
                if sortIndex > friends.count {
                    sortIndex = friends.count
                }
                friends.insert(Friend(name: userName, alias: alias, hasnedName: hash, sortedPosition: sortIndex), at: sortIndex)
                for i in 0...friends.count - 1 {
                    if friends[i].position != i {
                        friends[i].position = i
                    }
                }
                FriendData.friends = friends
            }
        }
    }

    func delete(friend: Friend) {
        var friendsList = FriendData.friends
        guard let index = friendsList.firstIndex(of: friend) else { return }
        friendsList.remove(at: index)
        FriendData.friends = friendsList
    }

}
