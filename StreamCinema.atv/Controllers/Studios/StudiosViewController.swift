//
//  StudiosViewController.swift
//  StreamCinema.atv
//
//  Created by iSCC on 23/12/2022.
//  Copyright © 2022 SCC. All rights reserved.
//

import UIKit
protocol StudiosViewControllerDelegate: AnyObject {
    func presentStaffMoviesViewController(for staff: MovieStaff)
}

final class StudiosViewController: UICollectionViewController {
    var data: [AZResult] = []
    weak var studioDelegate: StudiosViewControllerDelegate?
    internal var searchText: String = ""
    internal var filtredData: [AZResult] = []
    
    init() {
        let screenWidth = UIScreen.main.bounds.width
        let cellsCount:CGFloat = 6
        let space: CGFloat = 4
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .vertical
        let spacing = (space * (cellsCount + 1))
        layout.minimumInteritemSpacing = spacing
        layout.minimumLineSpacing = spacing
        layout.sectionInset = UIEdgeInsets(top: 0, left: spacing/2, bottom: 0, right: spacing/2)
        
        let cellWidth = screenWidth / cellsCount - (space * (cellsCount + 1))
        let bottomSpace = (cellWidth * (screenWidth - cellsCount)) / cellsCount
        
        layout.itemSize = CGSize(width: cellWidth, height: 300)
        
        super.init(collectionViewLayout: layout)
        collectionView.contentInset = .zero
        collectionView.register(StudiosCellView.self, forCellWithReuseIdentifier: "StudiosCellView")
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
    }
    
    internal func configureSubviews() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension StudiosViewController {
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudiosCellView", for: indexPath)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 willDisplay cell: UICollectionViewCell,
                                 forItemAt indexPath: IndexPath) {
        if let cell = cell as? StudiosCellView,
            let data = filtredData[safe: indexPath.row] {
            cell.set(data)
            cell.delegate = self
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filtredData.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let result = filtredData[safe: indexPath.row], let name = result.value else { return }
        let staff = MovieStaff(type: .studios, name: name, image: result.imageUrl)
        studioDelegate?.presentStaffMoviesViewController(for: staff)
    }
}

extension StudiosViewController: StudiosCellViewDelegate {
    func studiosCellView(_ view: StudiosCellView, didFound url: URL) {
        guard let index = data.firstIndex(where: { $0.value == view.data?.value }),
                let updatedData = view.data
        else { return }
        data[index] = updatedData
    }
}

extension StudiosViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text?.lowercased() else { return }
        let searchText = text.lowercased()
        
        if text.isEmpty {
            filtredData = data
        } else {
            filtredData = data.filter { vaule in
                if let name = vaule.value {
                    return name.lowercased().range(of: searchText) != nil
                }
                return false
            }
        }
        collectionView.reloadData()
    }
}
