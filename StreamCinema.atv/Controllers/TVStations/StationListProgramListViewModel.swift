//
//  StationListViewModel.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 27.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation
import Combine


/// This ViewModel is shared with 'StationListViewController' and 'StationProgramListViewCOntroller'.
/// They are on the split screen together.
final class StationListProgramListViewModel {

    let appData: AppData
    var errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()

    let tvStations:[TVStations] = TVStations.allCases
    var selectedStation:TVStations?
    var selectedDate: Date?

    var onReloadStationList: (() -> Void)?
    var onReloadProgramList: (() -> Void)?

    var selectedModel: FilterResult? {
        didSet {
            onReloadProgramList?()
        }
    }

    init(appData: AppData) {
        self.appData = appData
    }

    func numberOfStations() -> Int {
        return tvStations.count
    }

    func numberOfProgramItems() -> Int {
        return selectedModel?.hits.data?.count ?? 0
    }


    func station(for indexPath: IndexPath) -> TVStations? {
        return tvStations[safe: indexPath.row]
    }

    func program(for indexPath: IndexPath) -> InfoData? {
        return selectedModel?.hits.data?[safe: indexPath.row]
    }

    func select(station: TVStations) {
        selectedStation = station
        let actualDate = Date()
        selectedDate = actualDate
        if station == .All {
            self.updateData(date: actualDate)
        } else {
            self.updateData(station: station.rawValue, date: actualDate)
        }
    }


    func updateData(station:String = "", date:Date = Date(), page: Int = 1)  {
//        appData.scService
//            .getTvProgram(for: station, date: date.toDateString, page: page)
//            .assignError(to: errorSubject)
//            .sink { [weak self] filter in
//                if page == 0 {
//                    self?.selectedModel = filter
//                } else {
//                    self?.selectedModel?.hits.data?.append(contentsOf: filter.hits.data ?? [])
//                    self?.selectedModel?.hits.total.value = filter.hits.total.value
//                }
//            }.store(in: &cancelables)
    }

}
