//
//  TabBarViewController.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 18.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import SwiftUI
import Combine

final class TabBarViewController: UIViewController {

    // MARK: - Static propertiees
    static let vcIdentifier = "TabBarViewController"
    static func create(manager: TabBarManager) -> TabBarViewController {
        let storyboard = UIStoryboard.main
        let vc = storyboard.instantiateViewController(withIdentifier: vcIdentifier) as! TabBarViewController
        vc.manager = manager
        return vc
    }

    @IBOutlet weak var tabViewConteinerView: UIView!
    @IBOutlet weak var tabContollerContainerView: UIView!

    private var tabView: UIView?

    // MARK: - Private properties

    private let widthOfTabBar: CGFloat = 100
    private var manager: TabBarManager!


    private var tabVCDelegate: TabBarViewControllerDelegate {
        return manager as TabBarViewControllerDelegate
    }

    private var tabViewDelegate: TabBarViewDelegate {
        return manager as TabBarViewDelegate
    }

    override weak var preferredFocusedView: UIView? {
        if focusTabView {
            return tabView
        } else {
            return nil
        }
    }


    private var uiTabBarController: UITabBarController = UITabBarController()
#if canImport(TVUIKit)
    private var tabBarHostingController: UIHostingController<TabBarView>?
    #endif
    private var cancelables: Set<AnyCancellable> = Set()
    private var focusTabView: Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()

        setupMainController()
        setupTabViewContainer()
        setupTabBarViewControllerContainer()
        setupSubcriptions()

    }

    // MARK: Public methods

    func configure(viewControllers: [UIViewController]) {
        self.uiTabBarController.viewControllers = viewControllers
    }


    // MARK: - Setups methods

    private func setupSubcriptions() {

        tabVCDelegate
            .presentScreenPublisher
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: presentScreen(for:))
            .store(in: &cancelables)

        tabVCDelegate
            .popToRootEventPublisher
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: popToRoot(animated:))
            .store(in: &cancelables)

        NotificationCenter
            .Publisher(center: .default, name: .focusTabBar)
            .compactMap { $0.object as? Void }
            .sink(receiveValue: { [weak self] _ in
                self?.updateFocus()
            })
            .store(in: &cancelables)
    }

    private func setupMainController() {
        if let image = UIImage(named: "backgroundImage") {
            view.backgroundColor = UIColor(patternImage: image)
        }
    }

    private func updateFocus() -> Void {
        manager.needsUpdateFocus()
        focusTabView = true
        self.setNeedsFocusUpdate()
        self.updateFocusIfNeeded()
        focusTabView = false
    }


    private func setupTabViewContainer() {
        
#if canImport(TVUIKit)
        let tabViewHostingController = UIHostingController(rootView: TabBarView(width: widthOfTabBar, delegate: tabViewDelegate))
        addChild(tabViewHostingController)
        tabViewConteinerView.addSubview(tabViewHostingController.view)

        self.tabView = tabViewHostingController.view
        tabViewHostingController.view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            tabViewHostingController.view.leftAnchor.constraint(equalTo: tabViewConteinerView.leftAnchor),
            tabViewHostingController.view.topAnchor.constraint(equalTo: tabViewConteinerView.topAnchor),
            tabViewHostingController.view.bottomAnchor.constraint(equalTo: tabViewConteinerView.bottomAnchor),
            tabViewHostingController.view.rightAnchor.constraint(equalTo: tabViewConteinerView.rightAnchor)
        ])

        tabViewHostingController.view.frame = tabViewConteinerView.bounds

        tabViewHostingController.didMove(toParent: self)
        self.tabBarHostingController = tabViewHostingController
#endif
    }

    private func setupTabBarViewControllerContainer() {

        uiTabBarController.tabBar.isHidden = true
        addChild(uiTabBarController)
        tabContollerContainerView.addSubview(uiTabBarController.view)

        NSLayoutConstraint.activate([
            uiTabBarController.view.leftAnchor.constraint(equalTo: tabContollerContainerView.leftAnchor),
            uiTabBarController.view.rightAnchor.constraint(equalTo: tabContollerContainerView.rightAnchor),
            uiTabBarController.view.bottomAnchor.constraint(equalTo: tabContollerContainerView.bottomAnchor),
            uiTabBarController.view.topAnchor.constraint(equalTo: tabContollerContainerView.topAnchor)
        ])

        uiTabBarController.view.frame = tabContollerContainerView.bounds

        uiTabBarController.didMove(toParent: self)
        uiTabBarController.delegate = self

    }

    // MARK: - Private methods

    private func presentScreen(for index: Int) {
        uiTabBarController.selectedIndex = index
    }

    private func popToRoot(animated: Bool = true) {
        uiTabBarController.selectedViewController?
            .firstNavigationController?
            .popToRootViewController(animated: animated)
    }

}

extension TabBarViewController: UITabBarControllerDelegate {
//
//    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return FadeAnimation()
//    }
}
//

//fileprivate class FadeAnimation: NSObject, UIViewControllerAnimatedTransitioning {
//
//    let animationDuration = 0.2
//
//    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
//        return animationDuration
//    }
//
//    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
//        toVC?.view.alpha = 0.0
//        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
//        transitionContext.containerView.addSubview(fromVC!.view)
//        transitionContext.containerView.addSubview(toVC!.view)
//
//        UIView.animate(withDuration: animationDuration, animations: {
//            toVC?.view.alpha = 1.0
//        }) { (completed) in
//            fromVC?.view.removeFromSuperview()
//            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
//        }
//    }
//}
