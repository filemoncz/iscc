//
//  TraktQRViewController.swift
//  StreamCinema.atv
//
//  Created by SCC on 02/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import TraktKit

protocol TraktQRViewDelegate {
    func traktQRView(_ traktViewController:TraktQRViewController, isLogin success:Bool)
}

final class TraktQRViewController: UIViewController {
    var traktDelegate:TraktQRViewDelegate?
    private var data: DeviceCode?
    private let codeLabel: UILabel = UILabel()
    private let imageView: UIImageView = UIImageView()
    private let progress: UIProgressView = UIProgressView()
    private var alert: UIAlertController?
    private var progressView: UIProgressView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.start()
    }
    
    
    public func set(data: DeviceCode) {
        self.data = data
        self.imageView.image = data.getQRCode()
        self.codeLabel.numberOfLines = 0
        self.codeLabel.font = UIFont.systemFont(ofSize: 26, weight: .medium)
#if canImport(TVUIKit)
        self.codeLabel.text = String(format: String(localized: .trakt_qr_text), data.verificationURL, data.userCode)
        #endif
    }
    
    private func configureView() {
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.codeLabel)
        self.view.addSubview(self.progress)
        
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.codeLabel.translatesAutoresizingMaskIntoConstraints = false
        self.progress.translatesAutoresizingMaskIntoConstraints = false
        
        self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        self.imageView.heightAnchor.constraint(equalToConstant: 500).isActive = true
        self.imageView.widthAnchor.constraint(equalToConstant: 500).isActive = true
        
        self.codeLabel.centerXAnchor.constraint(equalTo: self.imageView.centerXAnchor).isActive = true
        self.codeLabel.topAnchor.constraint(equalTo: self.imageView.bottomAnchor , constant: 10).isActive = true
        self.codeLabel.widthAnchor.constraint(equalToConstant: 1000).isActive = true
        self.codeLabel.textAlignment = .center
        
        self.progress.centerXAnchor.constraint(equalTo: self.imageView.centerXAnchor).isActive = true
        self.progress.widthAnchor.constraint(equalToConstant: 500).isActive = true
        self.progress.topAnchor.constraint(equalTo: self.codeLabel.bottomAnchor, constant: 20).isActive = true
    }
    
    private func start() {
#if canImport(TVUIKit)
        guard let expireIn = self.data?.expiresIn else { return }
        TraktManager.sharedManager.getTokenFromDevice(code: data) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.traktDelegate?.traktQRView(self, isLogin: true)
                DispatchQueue.main.async {
                    self.startSync()
                }
            case .fail(let progressValue):
                if progressValue != 0 {
                    let value: Float = Float(progressValue) / Float(expireIn)
                    self.updateProgress(value)
                } else {
                    self.traktDelegate?.traktQRView(self, isLogin: false)
                    self.dismiss()
                }
            }
        }
        #endif
    }
    
    private func updateProgress(_ value:Float) {
        DispatchQueue.main.async {
            self.progress.progress = value
        }
    }
    
    private func dismiss() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func startSync() {
        self.alert = UIAlertController(title: String(localized: .wait), message: nil, preferredStyle: .alert)
        present(self.alert!, animated: true, completion: {
            //  Add your progressbar after alert is shown (and measured)
            let margin:CGFloat = 8.0
            let rect = CGRect(x: margin, y: 72.0, width: self.alert!.view.frame.width - margin * 2.0 , height: 2.0)
            self.progressView = UIProgressView(frame: rect)
            self.progressView!.progress = 0.0
            self.progressView!.tintColor = self.view.tintColor
            self.alert!.view.addSubview(self.progressView!)
        })
        let traktSync = (UIApplication.shared.delegate as? AppDelegate)?.appData.traktSync
        traktSync?.deleteSyncDate()
        traktSync?.start { [weak self] progress,error  in
            guard let self = self else { return }
            if error == nil {
                self.progressView?.progress = progress
                if progress == 100 {
                    self.showAlertForSyncEnd(title: String(localized: .sync_done))
                }
            } else {
                self.showAlertForSyncEnd(title: String(localized: .sync_false))
            }
        }
    }
    
    private func showAlertForSyncEnd(title:String) {
        self.alert?.dismiss(animated: false, completion: {
            let title = title
            self.alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            self.alert?.addAction(UIAlertAction(title: String(localized: .buttonOK), style: .cancel, handler: { [weak self] _ in
                self?.dismiss()
            }))
            self.present(self.alert!, animated: true, completion: nil)
        })
    }
    
}
