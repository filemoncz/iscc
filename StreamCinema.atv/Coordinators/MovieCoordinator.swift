//
//  TVShowCoordinator.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class MovieCoordinator: Coordinator {

    let rootViewController: UINavigationController
    private let appData: AppData

    // Child coordinators
    private var vlcCoordinator: VLCCoordinator?
    private var ksCoordinator: KSPlayerCoordinator?


    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
    }

    func start() {
        // Some data preperations
    }

    func createVC(for item: TabBarItem) -> UIViewController {
        let vc = createMovieListScreen(endpoint: item)
        vc.tabBarItem.title = item.description
        vc.tabBarItem.tag = item.rawValue
        return vc
    }

    func presentMovieList(for item: TabBarItem) {
        let vc = createMovieListScreen(endpoint: item)
        rootViewController.pushViewController(vc, animated: true)
    }

    func presentMovieAndShowViewController(movieData: SCCMovie) {
        let viewModel = MovieAndShowViewModel(movieData: movieData, appData: appData)
        let vc = MovieAndShowViewControllera.create(viewModel: viewModel)
        vc.delegate = self
        rootViewController.pushViewController(vc, animated: true)
    }

    private func presentActorsMoviesViewController(cast: Cast) {
        let vc = ActorMoviesViewController.create(appData: appData)
        vc.onPresentMovieDetailScreen = { [weak self] movie in
            self?.presentMovieAndShowViewController(movieData: movie)
        }
        vc.prepareData(with: cast)
        rootViewController.pushViewController(vc, animated: true)
    }
    
    private func presentStaffMoviesViewController(for staff: MovieStaff) {
        let vc = ActorMoviesViewController.create(appData: appData)
        vc.onPresentMovieDetailScreen = { [weak self] movie in
            self?.presentMovieAndShowViewController(movieData: movie)
        }
        vc.prepareData(for: staff)
        rootViewController.pushViewController(vc, animated: true)
    }

    private func presentVLCPlayer(modelData: MovieModel, isContinue: Bool = false,sourceView: UIView) {

        let vlcCoordinator = VLCCoordinator(appData: appData, rootViewController: rootViewController)
        self.vlcCoordinator = vlcCoordinator

        if modelData.type == .movie {
            vlcCoordinator.presentVLCPlayer(modelData: modelData, startAt: 0, isContinue: isContinue)
        } else {
            let currentSelected = modelData.selected
            guard currentSelected.episode <= modelData.episodes.count else { return }
            vlcCoordinator.presentVLCPlayer(modelData: modelData, startAt: currentSelected.episode, isContinue: isContinue)
        }
        vlcCoordinator.start(sourceView: sourceView)
    }
    
    private func presentKSPlayer(modelData: MovieModel ) {
        if let ksCoordinator = self.ksCoordinator {
            ksCoordinator.stop()
        }
        let ksCoordinator = KSPlayerCoordinator(appData: appData, rootViewController: rootViewController, modelData: modelData)
        self.ksCoordinator = ksCoordinator
        
        ksCoordinator.start()
    }

    private func createMovieListScreen(endpoint: TabBarItem) -> UIViewController {
        let vc = MovieListViewController.create(appData: appData)
        vc.onPresentMovieDetailScreen = { [weak self] model in
            self?.presentMovieAndShowViewController(movieData: model)
        }
        let menu: MenuViewController = MenuViewController()
        menu.endpoit = endpoint
        menu.menuDelegate = vc
        let split = UISplitViewController()
        split.viewControllers = [menu, vc]
        split.preferredPrimaryColumnWidthFraction = 2/7
        return split
    }

}


extension MovieCoordinator: MovieViewControllerDelegate {
    func presentDetailForCastScreen(for staff: MovieStaff) {
        presentStaffMoviesViewController(for: staff)
    }

    func presentCastDetailScreen(for cast: Cast) {
        self.presentActorsMoviesViewController(cast: cast)
    }

    func presentMovieDetailScreen(for relatedMovie: SCCMovie) {
        self.presentMovieAndShowViewController(movieData: relatedMovie)
    }

    func presentVideoPlayer(model: MovieModel, isContinue: Bool, sourceView: UIView) {
        self.presentVLCPlayer(modelData: model, isContinue: isContinue, sourceView: sourceView)
    }
    
    func presentHDRPlayer(model: MovieModel) {
        presentKSPlayer(modelData: model)
    }
}


import Combine
import KSPlayer

class MEOptions: KSOptions {
    override init() {
        super.init()
        KSOptions.canBackgroundPlay = true
        KSOptions.logLevel = .panic
        KSOptions.firstPlayerType = KSMEPlayer.self
        KSOptions.secondPlayerType = KSAVPlayer.self
        self.isAutoPlay = true
        self.isSecondOpen = true
        self.isAccurateSeek = true
        self.hardwareDecode = true
//        self.destinationDynamicRange = .hdr10
    }
    #if os(tvOS)
    override open func preferredDisplayCriteria(refreshRate: Float, videoDynamicRange: Int32) -> AVDisplayCriteria? {
         AVDisplayCriteria(refreshRate: refreshRate, videoDynamicRange: videoDynamicRange)
    }
    #endif
}

final class KSPlayerCoordinator: NSObject  {
    private let rootViewController: UINavigationController
    private let appData: AppData
    private var viewModel: VLCPlayerViewModel?
    private var playerController: KSPlayerViewController = KSPlayerViewController()
    private var cancelables: Set<AnyCancellable> = Set()

    init(appData: AppData, rootViewController: UINavigationController, modelData:MovieModel) {
        self.rootViewController = rootViewController
        self.appData = appData
        viewModel = VLCPlayerViewModel(
            modelData: modelData,
            startAt: 0,
            isContinue: false,
            appData: appData
        )
        super.init()
    }

    func start() {
        
        playerController.playTimeDidChange = { [weak self] currentTime, totalTime in
            self?.viewModel?.data.update(time: currentTime, totalTime: totalTime)
        }
        
        viewModel?.fetchStreams()
            .sink(receiveCompletion: { [weak self] completion in
                guard case .failure(let error) = completion else { return }
                self?.rootViewController.presentErrorAlert(error: error)
            }) { [weak self] (streams) in
                self?.playStreams(streams)
            }.store(in: &cancelables)
    }
    
    func stop() {
        rootViewController.popViewController(animated: true)
        playerController.stop()
    }
    
    internal func playStreams(_ streams: VideoStreams) {
        let sortedStreams = streams.getSortedData()
        let selectedStream = streams.isAutoSelectVideo(for: sortedStreams)
        
        Task {
            let streamsWithLinks = try? await sortedStreams.asyncCompactMap({ stream in
                if let url = try? await self.viewModel?.getAsyncLink(for: stream)?.createLink() {
                    var updatingStream = stream
                    updatingStream.palyableURL = url
                    updatingStream.isSelected = selectedStream == stream
                    return updatingStream
                }
                return nil
            })
            guard let streamsWithLinks else { return }
            if streamsWithLinks.count == 0 {
                await self.rootViewController.presentErrorAlert(error: RequestErrors.noDataForRequest)
                return
            }
            
            DispatchQueue.main.async {
                self.playMovie(streamsWithLinks)
            }
        }
    }
    
    private func playMovie(_ streams: [VideoStream], options: KSOptions = MEOptions()) {
        let resources = streams.compactMap { stream -> KSPlayerResourceDefinition? in
            guard let url = stream.palyableURL else { return nil }
            return KSPlayerResourceDefinition(url: url,
                                              definition: stream.getStreamInfo(isSselected: stream.isSelected),
                                              options:options)
        }
        let resource = KSPlayerResource(definitions: resources)
        rootViewController.pushViewController(playerController, animated: false)
        playerController.resource = resource
        playerController.playerView.change(definitionIndex: streams.firstIndex(where: { $0.isSelected }) ?? 0)
    }
}

final class KSPlayerViewController: UIViewController {
    #if os(tvOS)
    let playerView = MyTvOSVideoPlayerView()
    #elseif os(iOS)
    let playerView = IOSVideoPlayerView()
    #else
    let playerView = KSPlayerView()
    #endif
    
    public var playTimeDidChange: ((TimeInterval, TimeInterval) -> Void)?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        view.addSubview(playerView)
        
        playerView.translatesAutoresizingMaskIntoConstraints = false
        
        playerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        playerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        playerView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        playerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        playerView.resetPlayer()
    }
    
    var resource: KSPlayerResource? {
        didSet {
            if let resource {
                playerView.set(resource: resource)
                playerView.playTimeDidChange = { [weak self] currentTime, totalTime in
                    self?.playTimeDidChange?(currentTime,totalTime)
                }
            }
        }
    }
    
    func stop() {
        playerView.resetPlayer()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
}

#if os(tvOS)
class MyTvOSVideoPlayerView: TvOSVideoPlayerView {
    
    public override func player(layer: KSPlayerLayer, state: KSPlayerState) {
        super.player(layer: layer, state: state)
        switch state {
        case .readyToPlay:
            var isAudioConfigure = false
            if let tracks = playerLayer?.player.tracks(mediaType: .audio),
               let trackToSelect = tracks.first(where: { track in
                   let lang = track.language?.lowercased() ?? track.name.lowercased()
                   return audioSystemLang.contains(where: { $0.lowercased().contains(lang) })
               })
            {
                layer.player.select(track: trackToSelect)
                isAudioConfigure = true
            }
            if !isAudioConfigure {
                let availableSubtitles = srtControl.filterInfos { _ in true }
                guard availableSubtitles.count > 0,
                      let currentSub = srtControl.view.selectedInfo,
                      let srtToSelect = availableSubtitles.first(where: { srt in
                          return audioSystemLang.contains(where: { $0.lowercased().contains(srt.name.lowercased()) })
                      })
                else { return }
                
                if srtToSelect.name != currentSub.name {
                    srtControl.view.selectedInfo = srtToSelect
                }
            }
        default:
            break
        }
    }
    
    private lazy var audioSystemLang: [String] = {
        var languages: [String:[String]] = [:]
        languages["en"] = ["English", "EN", "ENG", "ANG"]
        languages["cs"] = ["Czech", "CZ", "CS", "CZE", "CES"]
        languages["sk"] = ["Slovak","Slovenština", "Slovensky", "SK", "SLO", "SLK"]
        
        if let lang = Locale.current.languageCode {
            return languages[lang] ?? []
        }
        return []
    }()
}
#endif
extension KSPlayerResource {
    convenience init?(url: URL?, options: KSOptions = MEOptions(), name: String) {
        guard let url else { return nil }
        self.init(url: url, options: options, name: name, cover: nil)
    }
}
