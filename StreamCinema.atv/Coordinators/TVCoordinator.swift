//
//  TVCoordinator.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 25.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import AVKit

final class TVCoordinator: Coordinator {
    let rootViewController: UINavigationController
    private let appData: AppData
    private var pleyerController: AVPlayerViewController?

    init(appData: AppData, rootViewController: UINavigationController) {
        self.rootViewController = rootViewController
        self.appData = appData
    }

    func start() {}

    func createVC(for tabBar: TabBarItem) -> UIViewController {
        let tvController = TVViewController.create(appData: appData)
        tvController.tvDelegate = self
        tvController.tabBarItem.title = tabBar.description
        return tvController
    }

    func presentTVScreen() {
        let tvController = TVViewController.create(appData: appData)
        tvController.tvDelegate = self
        rootViewController.pushViewController(tvController, animated: true)
    }
    
    private func presentVLCPlayer(panel: InfoPanelData, url: URL) {
        let asset = AVAsset(url: url)
        let item = AVPlayerItem(asset: asset)
        let player = AVPlayer(playerItem: item)
        let playerController = AVPlayerViewController()
        playerController.player = player
        rootViewController.pushViewController(playerController, animated: true)
        DispatchQueue.main.async { [weak player] in
            player?.play()
        }
    }
}

extension TVCoordinator: TVViewControllerDelegate {
    func presentVideoPlayer(tvLink: URL, title: String?, logo: URL?, desc: String?) {
        let panel = InfoPanelData(title: title ?? "", desc: desc ?? "" , posterURL: nil, bannerURL: nil, clearLogoURL: logo)
        self.presentVLCPlayer(panel: panel, url: tvLink)
    }
}
