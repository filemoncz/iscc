//
//  CommonExtensions.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 28.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import Kingfisher


extension UIStackView {
    func removeAllArrangedSubviews() {
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }

        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))

        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}



