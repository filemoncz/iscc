//
//  DateComponentsFormatter.swift
//  StreamCinema.atv
//
//  Created by SCC on 20/02/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import Foundation

public extension TimeInterval {
    func formatedString() -> String? {
        let formatter = DateComponentsFormatter()
        formatter.zeroFormattingBehavior = .pad
        formatter.unitsStyle = .abbreviated

        if self >= 3600 {
            formatter.allowedUnits = [.hour, .minute, .second]
        } else {
            formatter.allowedUnits = [.minute, .second]
        }
        return formatter.string(from: self)
    }
}
