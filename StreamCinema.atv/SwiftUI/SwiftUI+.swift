//
//  SwiftUI+.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 08.02.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI


extension View {

    func toAny() -> AnyView {
        return AnyView(self)
    }
}
