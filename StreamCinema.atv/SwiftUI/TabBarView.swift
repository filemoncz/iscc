//
//  TabView.swift
//  iSCC-iOS
//
//  Created by Martin Matějka on 17.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import SwiftUI
import Combine
import iSCCCore

struct TabBarView: View {

    var widht: CGFloat
    var delegate: TabBarViewDelegate

    private let topItems: [TabBarItem]
    private let bottomItems: [TabBarItem]

    @State private var selectedItem: TabBarItem
    @Namespace var focusNamespace
    @Namespace var focusViewNamespace
#if canImport(TVUIKit)
    @Environment(\.resetFocus) var resetFocus
    #endif
    @State private var isFocusedItem: TabBarItem = .movies

    private let itemHeight: CGFloat = 50


    init(width: CGFloat, delegate: TabBarViewDelegate) {
        self.delegate = delegate
        self.widht = width
        self._selectedItem = State(initialValue: delegate.selectedItem)
        let top = delegate.mainItems
        let bottom = delegate.bottomItems
        self.topItems = top.filter({ (item) -> Bool in
           !(item.isLast || item.isFirst)
        })
        self.bottomItems = bottom
    }

    var body: some View {

        VStack {

            VStack {
                TabBarItemView(item: .fullTextSearch,
                               isFocusedItem: $isFocusedItem,
                               selectedItem: selectedItem,
                               tabBarViewNamespace: _focusViewNamespace
                ) { item in
                    delegate.select(item: item)
                }
                .prefersDefaultFocus(selectedItem == .fullTextSearch, in: focusNamespace)
                .padding(.vertical, 15)

                VStack(alignment: .center) {
                    ForEach(topItems, id: \.rawValue) { item in
                        TabBarItemView(item: item,
                                       isFocusedItem: $isFocusedItem,
                                       selectedItem: selectedItem,
                                       tabBarViewNamespace: _focusViewNamespace
                        ) { item in
                            delegate.select(item: item)
                        }
                        .prefersDefaultFocus(selectedItem == item, in: focusNamespace)
                        .frame(height: itemHeight)
                        .padding(.vertical, 15)
                        .toAny()

                    }
                }

                TabBarItemView(item: .studios,
                               isFocusedItem: $isFocusedItem,
                               selectedItem: selectedItem,
                               tabBarViewNamespace: _focusViewNamespace
                ) { item in
                    delegate.select(item: item)
                }
                .prefersDefaultFocus(selectedItem == .studios, in: focusNamespace)
                .padding(.vertical, 15)
            }.frame(maxWidth: .infinity, maxHeight: .infinity)

            VStack {

                Color.green.opacity(0.6)
                    .frame(height: 2)
                    .frame(maxWidth: .infinity)
                    .padding(.horizontal, 20)

                ForEach(bottomItems, id: \.rawValue) { item in
                    TabBarItemView(item: item,
                                   isFocusedItem: $isFocusedItem,
                                   selectedItem: selectedItem,
                                   tabBarViewNamespace: _focusViewNamespace
                    ) { item in
                        delegate.select(item: item)
                    }
                    .prefersDefaultFocus(selectedItem == item, in: focusNamespace)
                }
            }.frame(height: 100)
        }
        .padding(.vertical, 20)
        .frame(width: widht, height: UIScreen.main.bounds.height, alignment: .center)
        .fixedSize(horizontal: true, vertical: true)
        .background(Color.black.opacity(0.6))
        .edgesIgnoringSafeArea(.all)
        .focusScope(focusNamespace)
        .onReceive(delegate.selectedItemPublisher, perform: { item in
            self.selectedItem = item
        })
        .onReceive(delegate.updateFocusEventPublisher, perform: { _ in
            #if canImport(TVUIKit)
            self.resetFocus(in: focusNamespace)
            #endif
        })
        .onAppear {
            #if canImport(TVUIKit)
            self.resetFocus(in: focusNamespace)
            #endif
        }
    }
}

struct CustomShape: Shape {

    var yAxis: CGFloat
    var itemWidth: CGFloat
    var extendedPadding: Bool
    // Animating Path...
    var extraScale: CGFloat = 0

    var animatableData: AnimatablePair<CGFloat, CGFloat> {
        get{ return AnimatablePair(yAxis, extendedPadding ? 1.3 : 1) }
        set{
            yAxis = newValue.first
            extraScale = newValue.second
        }
    }

    var itemHalf: CGFloat {
        itemWidth / 2
    }

    var width: CGFloat {
        15 * extraScale
    }

    var height: CGFloat {
        itemWidth * extraScale
    }

    var extraY: CGFloat {
        guard extraScale > 1 else { return 0 }
        return 5
    }


    func path(in rect: CGRect) -> Path {

        return Path { path in

            let center = yAxis

            path.addRoundedRect(in: .init(x: -10, y: center - itemHalf - extraY, width: width, height: height), cornerSize: .init(width: 10, height: 10))

        }
    }
}


struct CornerRadiusStyle: ViewModifier {
    var radius: CGFloat
    var corners: UIRectCorner

    struct CornerRadiusShape: Shape {

        var radius = CGFloat.infinity
        var corners = UIRectCorner.allCorners

        func path(in rect: CGRect) -> Path {
            let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            return Path(path.cgPath)
        }
    }

    func body(content: Content) -> some View {
        content
            .clipShape(CornerRadiusShape(radius: radius, corners: corners))
    }
}

extension View {
    func cornerRadius(radius: CGFloat, corners: UIRectCorner) -> some View {
        ModifiedContent(content: self, modifier: CornerRadiusStyle(radius: radius, corners: corners))
    }
}
