//
//  MenuItem.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/06/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

enum MenuItem {
    case popular
    case trending
    case mostWatched
    case news
    case dubbed
    case lastAdded
    case lastAddedDubbed
    case watched
    case watchedList
    case christmas
    case traktWatchList
    case genre
    case traktHistory
    case friendsAdvise

    var description: String {
        switch  self {
        case .mostWatched:
            return String(localized: .most_watched)
        case .dubbed:
            return String(localized: .new_dubbed_releases)
        case .trending:
            return String(localized: .trending)
        case .lastAdded:
            return String(localized: .recently_added)
        case .news:
            return String(localized: .new_releases)
        case .popular:
            return String(localized: .popular)
        case .watched:
            return String(localized: .watch_history)
        case .watchedList:
            return String(localized: .watch_list)
        case .traktWatchList:
            return String(localized: .watch_list)
        case .traktHistory:
            return String(localized: .watch_history)
        case .genre:
            return String(localized: .genre)
        case .lastAddedDubbed:
            return String(localized: .recently_dubbed_added)
        case .christmas:
            return String(localized: .christmas)
        case .friendsAdvise:
            return String(localized: .friendsAdvise)
        }
    }
}
