//
//  MovieRating.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit

public struct Rating {
    var csfd: String?
    var imdb: String?
    var trakt: String?
    var tmdb: String?
    
    func ratingTitle() -> String? {
        if let csfd = self.csfd {
            return " CSFD: \(csfd) "
        }
        else if let imdb = self.imdb {
            return " IMDB: \(imdb) "
        }
        else if let trakt = self.trakt {
            return " TRAKT: \(trakt) "
        }
        else if let tmdb = self.tmdb {
            return " TMDB: \(tmdb) "
        }
        return nil
    }
    
    func allRatings() -> NSMutableAttributedString {
        let rating:NSMutableAttributedString = NSMutableAttributedString(string: "")
        if let csfd = self.csfd {
            rating.append(self.rating("CSFD", csfd))
        }
        else if let imdb = self.imdb {
            rating.append(self.rating("IMDB", imdb))
        }
        else if let trakt = self.trakt {
            rating.append(self.rating("TRAKT", trakt))
        }
        else if let tmdb = self.tmdb {
            rating.append(self.rating("TMDB", tmdb))
        }
        return rating
    }
    
    private func rating(_ key: String, _ value: String) -> NSMutableAttributedString {
        let keyStr = NSMutableAttributedString(string: "\(key): ",
                                            attributes: [.foregroundColor:UIColor.label,
                                                         NSAttributedString.Key.font:UIFont.systemFont(ofSize: 20, weight: .medium)])
        let valueStr = NSMutableAttributedString(string: "\(value)",
                                            attributes: [.foregroundColor:UIColor.label,
                                                         NSAttributedString.Key.font:UIFont.systemFont(ofSize: 17)])
        keyStr.append(valueStr)
        return keyStr
    }
}

extension SourceInfo {
    func getRating() -> Rating {
        var rating = Rating()
        if let ratings = self.ratings {
            for item in ratings {
                guard let currentRating = item.value.rating else { continue }
                if item.key == "csfd" {
                    rating.csfd = "\(Int(currentRating * 10)) %"
                } else if item.key == "imdb" {
                    rating.imdb = "\(Int(currentRating * 10)) %"
                } else if item.key == "tmdb" {
                    rating.tmdb = "\(Int(currentRating * 10)) %"
                } else if item.key == "trakt" {
                    rating.trakt = "\(Int(currentRating * 10)) %"
                }
            }
        }
        return rating
    }
}
