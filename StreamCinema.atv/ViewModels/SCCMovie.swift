//
//  SCCMovie.swift
//  StreamCinema.atv
//
//  Created by SCC on 19/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import TraktKit
import FanArtKit
import TMDBKit
import CoreData

public enum SCCType: String {
    case movie
    case episode
    case tvshow
    case season
}

extension MediaType {
    var sccT: SCCType {
        switch self {
        case .movie:
            return .movie
        case .tvshow:
            return .tvshow
        case .season:
            return .season
        case .episode:
            return .episode
        }
    }
}

public struct SCCMovie: Equatable {
    var title:String
    var traktType:SCCType
    var rating:Rating?
    var auiodLangs:[String]?
    
    var rootIDs:IDs?
    var seasonIDs:IDs?
    var episodeIDs:IDs?
    var season:Int?
    var episode:Int?
    
    var poster: URL?
    var banner: URL?
    var fanart: URL?
    var logo: URL?
    var thumb: URL?
    var date: Date?
    
    //SCC data:
    var trailer: String?
    var desc: String?
    var movieData: [[String : String]]?
    var state: WatchedState?
    
    init() {
        self.title = "NONE"
        self.traktType = .movie
    }
    
    init(title: String, traktType: SCCType, rating: Rating?, auiodLangs: [String]?, rootIDs: IDs?, seasonIDs: IDs?, episodeIDs: IDs?, season: Int?, episode: Int?, poster: URL?, banner: URL?, fanart: URL?, logo: URL?, thumb: URL?, date: Date?, trailer: String?, desc: String?, movieData: [[String : String]], state: WatchedState?) {
        self.title = title
        self.traktType = traktType
        self.rating = rating
        self.auiodLangs = auiodLangs
        self.rootIDs = rootIDs
        self.seasonIDs = seasonIDs
        self.episodeIDs = episodeIDs
        self.season = season
        self.episode = episode
        self.poster = poster
        self.banner = banner
        self.fanart = fanart
        self.logo = logo
        self.thumb = thumb
        self.date = date
        self.trailer = trailer
        self.desc = desc
        self.movieData = movieData
        self.state = state
    }
    
    init(with data:NSManagedObject) {
        self.title = data.value(forKey: "title") as? String ?? ""
        self.episode = data.value(forKey: "eipsodeNumber") as? Int ?? 0
        self.season = data.value(forKey: "seasonNumber") as? Int ?? 0
        self.date = data.value(forKey: "traktDate") as? Date

        if let typeString = data.value(forKey: "type") as? String,
           let type = SCCType(rawValue: typeString) {
            self.traktType = type
        } else {
            self.traktType = .movie
        }

        if self.traktType == .movie {
            self.rootIDs = IDs(with: data, for: .movie)
        } else {
            self.rootIDs = IDs(with: data, for: .tvshow)
            self.seasonIDs = IDs(with: data, for: .season)
            self.episodeIDs = IDs(with: data, for: .episode)
        }

    }

    init(traktItem:TraktHistoryItem, type:TraktKit.WatchedType? = nil) {
        if let movie = traktItem.movie {
            self.title = movie.title
            self.traktType = .movie
            self.rootIDs = IDs(movie.ids)
            self.rating = Rating()
            if let rating = movie.rating {
                self.rating?.trakt = "\(Int(rating * 10)) %"
            }
        }
        else if let episode = traktItem.episode {
            self.title = episode.title ?? "\(episode.number) - \(String(localized: .episode))"
            self.traktType = .episode
            self.episode = episode.number
            self.season = episode.season

            if let rating = episode.rating {
                self.rating?.trakt = "\(Int(rating * 10))%"
            }

            //IDs
            self.rootIDs = traktItem.show?.ids.getIDs()
            self.seasonIDs = traktItem.season?.ids.getIDs()
            self.episodeIDs = traktItem.episode?.ids.getIDs()
        }
        else {
            self.title = ""
            self.traktType = .movie
        }
    }

    init(listItem:TraktListItem) {
        if let movie = listItem.movie {
            self.traktType = .movie
            self.title = movie.title
            self.rootIDs = movie.ids.getIDs()
            if let rating = movie.rating {
                self.rating?.trakt = "\(Int(rating * 10)) %"
            }
        }
        else if let episode = listItem.episode {
            self.title = episode.title ?? "\(episode.number) - \(String(localized: .episode))"
            self.traktType = .episode
            self.episode = episode.number
            self.season = episode.season
            if let rating = episode.rating {
                self.rating?.trakt = "\(Int(rating * 10))%"
            }
            //IDs
            self.rootIDs = listItem.show?.ids.getIDs()
            self.seasonIDs = listItem.season?.ids.getIDs()
            self.episodeIDs = listItem.episode?.ids.getIDs()
        }
        else {
            self.title = ""
            self.traktType = .movie
        }
    }
    
    public static func == (lhs: SCCMovie, rhs: SCCMovie) -> Bool {
        switch lhs.traktType {
        case .tvshow,
             .movie:
            return lhs.rootIDs == rhs.rootIDs
        case .episode:
            return lhs.episodeIDs == rhs.episodeIDs
        case .season:
            return lhs.seasonIDs == rhs.seasonIDs
        }
    }
}
//MARK: - mearge objects
extension SCCMovie {
    public mutating func mearge(with object:SCCMovie) {
        if self.title != object.title  {
            self.title = object.title
        }
        if self.desc != object.desc, object.desc != nil {
            self.desc = object.desc
        }
        
        self.auiodLangs = object.auiodLangs
        self.traktType = object.traktType
        self.rating = object.rating
        
        if object.rootIDs != nil {
            self.rootIDs = object.rootIDs
        }
        if object.seasonIDs != nil {
            self.seasonIDs = object.seasonIDs
        }
        if object.episodeIDs != nil {
            self.episodeIDs = object.episodeIDs
        }

        if object.season != nil {
            self.season = object.season
        }
        if object.season != nil {
            self.season = object.season
        }
        
        self.poster = object.poster
        self.banner = object.banner
        self.fanart = object.fanart
        self.logo = object.logo
        self.thumb = object.thumb
        
        if object.date != nil {
            self.date = object.date
        }
        
        self.trailer = object.trailer
        self.movieData = object.movieData
        self.state = object.state
    }
}

//MARK: - SCC data parse
extension InfoData {
    func getSCCMovie() -> SCCMovie {
        guard let id = self.id,
              let infoLabel = self.source?.getInfoLabels(),
              let type = self.source?.infoLabels?.getMediaType()
        else { return SCCMovie() }
        let state = SccWatchedData.getWatched(by: id, isTvShow: type != .movie)?.state
        
        let desc = infoLabel.plot
        var title = infoLabel.title
        let currentIDs = self.source?.services?.getIDs(with: id)
        var rootIDs:IDs?
        var seasonIDs:IDs?
        var episodeIDs:IDs?
        
        switch type {
        case .movie,
             .tvshow:
            rootIDs = currentIDs
        case .season:
            if title == nil {
                title = String(localized: .season)
                if let seasionNumber = self.source?.infoLabels?.season {
                    title = "\(seasionNumber) - \(String(localized: .season))"
                }
            }
            seasonIDs = currentIDs
        case .episode:
            if title == nil {
                title = String(localized: .episode)
                if let episodeNumber = self.source?.infoLabels?.episode {
                    title = "\(episodeNumber) - \(String(localized: .episode))"
                }
            }
            episodeIDs = currentIDs
        }
        
        var posterUrl:URL?
        if let posterString = infoLabel.art?.poster,
           !posterString.contains("null") {
            posterUrl = URL(string: posterString)
        }
        
        var bannerUrl:URL?
        if let bannerString = infoLabel.art?.banner,
           !bannerString.contains("null") {
            bannerUrl = URL(string: bannerString)
        }
        
        var fanArtURL:URL?
        if let fanartString = infoLabel.art?.fanart,
           !fanartString.contains("null") {
            fanArtURL = URL(string: fanartString)
        }
        
        var clearlogoURL:URL?
        if let clearlogoString = infoLabel.art?.clearlogo,
           !clearlogoString.contains("null") {
            clearlogoURL = URL(string: clearlogoString)
        }
        
        let audio = self.source?.availableStreams?.languages?.audio?.map
        
        let movieData = self.source?.getMovieData() ?? [[:]]
        let ratingData = self.source?.getRating() ?? Rating()
        let trailer = self.source?.getTrailer()
        let episode = self.source?.infoLabels?.episode
        let season = self.source?.infoLabels?.season
        
        return SCCMovie(title: title!, traktType: type.sccT, rating: ratingData, auiodLangs: audio,
                        rootIDs: rootIDs, seasonIDs: seasonIDs, episodeIDs: episodeIDs,
                        season: season, episode: episode, poster: posterUrl, banner: bannerUrl, fanart: fanArtURL, logo: clearlogoURL, thumb: nil, date: nil, trailer: trailer, desc: desc, movieData: movieData, state: state)
    }
}

//MARK: - Core Data
extension SCCMovie {
    private static func getContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.viewContext
    }
    
    private static func getContainer() -> NSPersistentStoreCoordinator? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.persistentStoreCoordinator
    }
    
    public func save(_ traktItem:SCCMovie, type: SCCType, managedContext: NSManagedObjectContext) {
        if !traktItem.exist(context: managedContext) {
            traktItem.save(context: managedContext)
        }
    }

    private func updateObject(_ object: NSManagedObject)  {
        object.setValue(self.title, forKey: "title")
        object.setValue(self.episode, forKey: "eipsodeNumber")
        object.setValue(self.episodeIDs?.csfd, forKey: "episodeCsfd")
        object.setValue(self.episodeIDs?.imdb, forKey: "episodeImdb")
        object.setValue(self.episodeIDs?.sccID, forKey: "episodeScc")
        object.setValue(self.episodeIDs?.tmdb, forKey: "episodeTmdb")
        object.setValue(self.episodeIDs?.trakt, forKey: "episodeTrakt")
        object.setValue(self.episodeIDs?.tvdb, forKey: "episodeTvdb")
        object.setValue(self.seasonIDs?.csfd, forKey: "seasonCsfd")
        object.setValue(self.seasonIDs?.imdb, forKey: "seasonImdb")
        object.setValue(self.season, forKey: "seasonNumber")
        object.setValue(self.seasonIDs?.sccID, forKey: "seasonScc")
        object.setValue(self.seasonIDs?.tmdb, forKey: "seasonTmdb")
        object.setValue(self.seasonIDs?.trakt, forKey: "seasonTrakt")
        object.setValue(self.seasonIDs?.tvdb, forKey: "seasonTvdb")
        object.setValue(self.rootIDs?.csfd, forKey: "showCsfd")
        object.setValue(self.rootIDs?.sccID, forKey: "showScc")
        object.setValue(self.rootIDs?.tmdb, forKey: "showTmdb")
        object.setValue(self.rootIDs?.trakt, forKey: "showTrakt")
        object.setValue(self.rootIDs?.tvdb, forKey: "showTvdb")
        object.setValue(self.date, forKey: "traktDate")
        object.setValue(self.traktType.rawValue, forKey: "type")
        object.setValue(Date(), forKey: "syncDate")
    }
    
    func save(context:NSManagedObjectContext) {
        let traktEntity = NSEntityDescription.insertNewObject(forEntityName: "TraktEpisodes",
                                                              into: context)
        self.updateObject(traktEntity)
        do {
            try context.save()
            print("Success")
        } catch {
            print("Error saving: \(error)")
        }
    }
    
    func exist(context:NSManagedObjectContext) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TraktEpisodes")

        if self.traktType == .movie,
           let traktID = self.rootIDs?.trakt {
            request.predicate = NSPredicate(format: "showTrakt = %d", traktID)
        } else if self.traktType == .episode,
                  let traktID = self.episodeIDs?.trakt{
            request.predicate = NSPredicate(format: "episodeTrakt = %d", traktID)
        } else {
            return false
        }

        request.returnsObjectsAsFaults = false
        do {
            let count = try context.count(for: request)
            if count > 0 {
                return true
            }
        } catch let error {
            print(error)
        }
        return false
    }
}

//MARK: - Fanart and TMDB images
extension SCCMovie {
    public func cellBacgroundImage(completition: @escaping (URL?)->Void) {
        if let thumb = self.thumb {
            completition(thumb)
        }
        else if let fanArt = self.fanart {
            completition(fanArt)
        } else {
            self.fetchBacgroundImage(completition: completition)
        }
    }
    
    private func fetchBacgroundImage(completition: @escaping (URL?)->Void) {
        self.tmdbBackdrop { url in
            if let url = url {
                completition(url)
            } else {
                self.fanArtThumb(completition: completition)
            }
        }
    }
    
    private func tmdbBackdrop(completition: @escaping (URL?)->Void ) {
        if let tmdbID = self.rootIDs?.tmdb {
            if self.traktType == .movie {
                MovieMDB.images(movieID: tmdbID, language: nil) { (value, images) in
                    self.tmdbImage(value, images, completition: completition)
                }
            } else if self.traktType == .episode,
                      let tmdb = self.rootIDs?.tmdb,
                      let episode = self.episode,
                      let season = self.season {
                TVEpisodesMDB.images(tvShowId: tmdb, seasonNumber: season, episodeNumber: episode) { (value, images) in
                    self.tmdbImage(value, images, completition: completition)
                }
            } else {
                TVMDB.images(tvShowID: tmdbID, language: nil) { (value, images) in
                    self.tmdbImage(value, images, completition: completition)
                }
            }
        }
    }
    
    private func tmdbImage(_ value: ClientReturn, _ images: ImagesMDB?, completition: @escaping (URL?)->Void ) {
        if let backdrop = value.json?["backdrop_path"],
           backdrop != nil,
           let backdropURL = URL(string: "https://image.tmdb.org/t/p/w500\(backdrop)"){
            completition(backdropURL)
        }
        else if let thumb = images?.backdrops.first?.file_path,
           let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(thumb)") {
            completition(backdrop)
        }
        else if let stillPath = images?.stills.first?.file_path,
                let backdrop = URL(string: "https://image.tmdb.org/t/p/w500\(stillPath)") {
            completition(backdrop)
        }
        else {
            completition(nil)
        }
    }
    
    private func fanArtThumb(completition: @escaping (URL?)->Void) {
        if let tmdbID = self.rootIDs?.tmdb {
            if self.traktType == .movie {
                FanartClient.shared.movie(tmdb: tmdbID) { fanArtResult -> (Void) in
                    switch fanArtResult {
                    case .success(result: let result):
                        print(result)
                        if let strURL = result.moviethumb?.first?.url {
                            completition(URL(string: strURL))
                        } else {
                            completition(nil)
                        }
                    case .error(reason: let reason):
                        print("getFanArt reason \(reason)")
                        completition(nil)
                    }
                }
            } else {
                FanartClient.shared.show(identifier: tmdbID) { fanArtResult -> (Void) in
                    switch fanArtResult {
                    case .success(result: let result):
                        print(result)
                        if let strURL = result.tvthumb?.first?.url {
                            completition(URL(string: strURL))
                        } else {
                            completition(nil)
                        }
                    case .error(reason: let reason):
                        print("getFanArt reason \(reason)")
                        completition(nil)
                    }
                }
            }
        } else {
            completition(nil)
        }
    }
}


