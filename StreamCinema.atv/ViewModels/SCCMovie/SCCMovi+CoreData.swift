//
//  SCCMovi+CoreData.swift
//  StreamCinema.atv
//
//  Created by SCC on 30/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import CoreData

//MARK: - Core Data
extension SCCMovie {
    private func getContext() -> NSManagedObjectContext? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.viewContext
    }
    
    private func getContainer() -> NSPersistentStoreCoordinator? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
        return appDelegate.persistentContainer.persistentStoreCoordinator
    }
    
    public func addToSccWatchList() {
        if let context = self.getContext() {
            if self.existWithTraktID(context: context, entity: WatchedEntity.sccWatchList) {
                self.update(context: context, entity: WatchedEntity.sccWatchList)
            } else {
                self.save(context: context, entity: WatchedEntity.sccWatchList)
            }
        }
    }
    
    public func removeFromSccWatchList() {
        if let context = self.getContext() {
            if let objectToDelete = self.existObjectWithTraktID(context: context, entity: WatchedEntity.sccWatchList) {
                self.delete(object:objectToDelete, in: context, entity: WatchedEntity.sccWatchList)
            }
        }
    }
    
    public func save(_ traktItem:SCCMovie, type: SCCType, managedContext: NSManagedObjectContext, entity: WatchedEntity) {
        if !traktItem.existWithTraktID(context: managedContext, entity: entity) {
            traktItem.save(context: managedContext, entity: entity)
        }
    }

    private func updateObject(_ object: NSManagedObject)  {
        object.setValue(self.title, forKey: "title")
        object.setValue(self.episode, forKey: "eipsodeNumber")
        object.setValue(self.episodeIDs?.csfd, forKey: "episodeCsfd")
        object.setValue(self.episodeIDs?.imdb, forKey: "episodeImdb")
        object.setValue(self.episodeIDs?.sccID, forKey: "episodeScc")
        object.setValue(self.episodeIDs?.tmdb, forKey: "episodeTmdb")
        object.setValue(self.episodeIDs?.trakt, forKey: "episodeTrakt")
        object.setValue(self.episodeIDs?.tvdb, forKey: "episodeTvdb")
        object.setValue(self.seasonIDs?.csfd, forKey: "seasonCsfd")
        object.setValue(self.seasonIDs?.imdb, forKey: "seasonImdb")
        object.setValue(self.season, forKey: "seasonNumber")
        object.setValue(self.seasonIDs?.sccID, forKey: "seasonScc")
        object.setValue(self.seasonIDs?.tmdb, forKey: "seasonTmdb")
        object.setValue(self.seasonIDs?.trakt, forKey: "seasonTrakt")
        object.setValue(self.seasonIDs?.tvdb, forKey: "seasonTvdb")
        object.setValue(self.rootIDs?.csfd, forKey: "tvshowCsfd")
        object.setValue(self.rootIDs?.sccID, forKey: "tvshowScc")
        object.setValue(self.rootIDs?.tmdb, forKey: "tvshowTmdb")
        object.setValue(self.rootIDs?.trakt, forKey: "tvshowTrakt")
        object.setValue(self.rootIDs?.tvdb, forKey: "tvshowTvdb")
        object.setValue(self.date, forKey: "traktDate")
        object.setValue(self.traktType.rawValue, forKey: "type")
        object.setValue(self.time, forKey: "time")
        object.setValue(self.progress, forKey: "progress")
        object.setValue(Date(), forKey: "syncDate")
    }
    
    func saveOrUpdate(_ entity: WatchedEntity = .scc) {
        if let context = self.getContext() {
            if self.existWithTraktID(context: context, entity: entity) {
                self.update(context: context, entity: entity)
            } else {
                self.save(context: context, entity: entity)
            }
        }
    }
    
    func save(context:NSManagedObjectContext, entity: WatchedEntity) {
        let traktEntity = NSEntityDescription.insertNewObject(forEntityName: entity.rawValue,
                                                              into: context)
        self.updateObject(traktEntity)
        do {
            try context.save()
            Log.write("SCCMovie save context Success")
        } catch {
            Log.write("SCCMovie save context error: \(error)")
        }
    }
    
    private func delete(object: NSManagedObject ,in context:NSManagedObjectContext, entity: WatchedEntity) {
        context.delete(object)
        try? context.save()
    }
    
    private func update(context:NSManagedObjectContext, entity: WatchedEntity) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)
        
        if (entity == .scc || entity == .sccWatchList),
           let predicate = getSccIDPredicate() {
            request.predicate = predicate
        } else if entity == .trakt,
                  let predicate = getTraktIDPredicate() {
            request.predicate = predicate
        } else {
            return
        }

        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if let objectUpdate = result[safe: 0] as? NSManagedObject {
                self.updateObject(objectUpdate)
            } else {
                return
            }
            
            try context.save()
            
        } catch let error {
            Log.write("SCCMovie update context error: \(error)")
        }
    }
    
    func existWithTraktID(context:NSManagedObjectContext, entity:WatchedEntity) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)

        if entity == .sccWatchList {
            request.predicate = self.getSccIDPredicate()
        } else {
            request.predicate = self.getTraktIDPredicate()
        }
        if request.predicate == nil {
            return false
        }

        request.returnsObjectsAsFaults = false
        do {
            let count = try context.count(for: request)
            if count > 0 {
                return true
            }
        } catch let error {
            Log.write("SCCMovie existWithTraktID: \(error)")
        }
        return false
    }
    
    func existObjectWithTraktID(context:NSManagedObjectContext, entity:WatchedEntity) -> NSManagedObject? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity.rawValue)

        if entity == .sccWatchList {
            request.predicate = self.getSccIDPredicate()
        } else {
            request.predicate = self.getTraktIDPredicate()
        }
        if request.predicate == nil {
            return nil
        }

        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            if let objectUpdate = result[safe: 0] as? NSManagedObject {
                return objectUpdate
            }
        } catch let error {
            Log.write("SCCMovie existObjectWithTraktID: \(error)")
        }
        return nil
    }
    
    private func getTraktIDPredicate() -> NSPredicate? {
        if self.traktType == .movie,
           let traktID = self.ids?.trakt {
            return NSPredicate(format: "tvshowTrakt = %d", traktID)
        } else if self.traktType == .episode,
                  let traktID = self.ids?.trakt {
            return NSPredicate(format: "episodeTrakt = %d", traktID)
        } else if self.traktType == .tvshow,
                 let traktID = self.ids?.trakt {
            return NSPredicate(format: "tvshowTrakt = %d", traktID)
        }
        return nil
    }
    
    private func getSccIDPredicate() -> NSPredicate? {
        if self.traktType == .movie,
           let sccID = self.ids?.sccID {
            return NSPredicate(format: "tvshowScc = %@", sccID)
        } else if self.traktType == .episode,
                  let sccID = self.ids?.sccID {
            return NSPredicate(format: "episodeScc = %@", sccID)
        } else if self.traktType == .tvshow,
                 let sccID = self.ids?.sccID {
            return NSPredicate(format: "tvshowScc = %@", sccID)
        }
        return nil
    }
}
