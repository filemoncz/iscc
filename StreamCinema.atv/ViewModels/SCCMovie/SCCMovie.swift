//
//  SCCMovie.swift
//  StreamCinema.atv
//
//  Created by SCC on 19/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import TraktKit
import FanArtKit
import TMDBKit
import CoreData

public enum WatchedState {
    case none
    case paused
    case done
    case trakt
}

public enum SCCType: String {
    case movie
    case episode
    case tvshow
    case season
    
    var filterType: FilterType {
        switch self {
        case .movie:
            return .movie
        case .tvshow:
            return .tvshow
        case .season:
            return .season
        case .episode:
            return .episode
        }
    }
    
    var media: MediaType {
        switch self {
        case .movie:
            return .movie
        case .tvshow:
            return .tvshow
        case .season:
            return .season
        case .episode:
            return .episode
        }
    }
}

extension MediaType {
    var sccT: SCCType {
        switch self {
        case .movie:
            return .movie
        case .tvshow:
            return .tvshow
        case .season:
            return .season
        case .episode:
            return .episode
        }
    }
}

public struct SCCMovie: Equatable {
    var title:String
    var traktType:SCCType
    var rating:Rating?
    var auiodLangs:[String]?
    
    var rootIDs:IDs?
    var seasonIDs:IDs?
    var episodeIDs:IDs?
    var season:Int?
    var episode:Int?
    
    var poster: URL?
    var banner: URL?
    var fanart: URL?
    var logo: URL?
    var thumb: URL?
    var date: Date?
    
    var progress: Double?
    var time: Double?
    
    var totalCount: Int
    
    //SCC data:
    var trailer: String?
    var desc: String?
    var movieData: [[String : String]]?
    var state: WatchedState?
    var cast: [Cast]?
    var year: Int?
    var director: [String]?
    var studio: [String]?
    var writers: [String]?
    
    var ids:IDs? {
        get {
            switch self.traktType {
            case .movie,
                 .tvshow:
                return self.rootIDs
            case .episode:
                return self.episodeIDs
            case .season:
                return self.seasonIDs
            }
        }
    }

    init() {
        self.title = "NONE"
        self.traktType = .movie
        self.totalCount = 0
    }
    
    init(title: String, traktType: SCCType, rating: Rating?, auiodLangs: [String]?, rootIDs: IDs?, seasonIDs: IDs?,
         episodeIDs: IDs?, season: Int?, episode: Int?, poster: URL?, banner: URL?, fanart: URL?, logo: URL?,
         thumb: URL?, date: Date?, trailer: String?, desc: String?, movieData: [[String : String]],
         state: WatchedState?, cast:[Cast]?,progress: Double?, time: Double?, year:Int?, director: [String]?,
         studio: [String]?, writers: [String]?, totalCount: Int)
    {
        self.title = title
        self.traktType = traktType
        self.rating = rating
        self.auiodLangs = auiodLangs
        self.rootIDs = rootIDs
        self.seasonIDs = seasonIDs
        self.episodeIDs = episodeIDs
        self.season = season
        self.episode = episode
        self.poster = poster
        self.banner = banner
        self.fanart = fanart
        self.logo = logo
        self.thumb = thumb
        self.date = date
        self.trailer = trailer
        self.desc = desc
        self.movieData = movieData
        self.state = state
        self.cast = cast
        self.time = time
        self.progress = progress
        self.year = year
        self.studio = studio
        self.director = director
        self.writers = writers
        self.totalCount = totalCount
    }
    
    init(with data:NSManagedObject) {
        self.title = data.value(forKey: "title") as? String ?? ""
        self.episode = data.value(forKey: "eipsodeNumber") as? Int ?? 0
        self.season = data.value(forKey: "seasonNumber") as? Int ?? 0
        self.date = data.value(forKey: "traktDate") as? Date
        self.progress = data.value(forKey: "progress") as? Double
        self.time = data.value(forKey: "time") as? Double
        self.totalCount = 0

        if let typeString = data.value(forKey: "type") as? String,
           let type = SCCType(rawValue: typeString) {
            self.traktType = type
        } else {
            self.traktType = .movie
        }

        if self.traktType == .movie {
            self.rootIDs = IDs(with: data, for: .movie)
        } else {
            self.rootIDs = IDs(with: data, for: .tvshow)
            self.seasonIDs = IDs(with: data, for: .season)
            self.episodeIDs = IDs(with: data, for: .episode)
        }

    }

    init(traktItem:TraktHistoryItem, type:TraktKit.WatchedType? = nil) {
        if let movie = traktItem.movie {
            self.title = movie.title
            self.traktType = .movie
            self.rootIDs = IDs(movie.ids)
            self.rating = Rating()
            if let rating = movie.rating {
                self.rating?.trakt = "\(Int(rating * 10)) %"
            }
            self.year = movie.year
        }
        else if let episode = traktItem.episode {
            self.title = episode.title ?? "\(episode.number) - \(String(localized: .episode))"
            self.traktType = .episode
            self.episode = episode.number
            self.season = episode.season

            if let rating = episode.rating {
                self.rating?.trakt = "\(Int(rating * 10))%"
            }

            //IDs
            self.rootIDs = traktItem.show?.ids.getIDs()
            self.seasonIDs = traktItem.season?.ids.getIDs()
            self.episodeIDs = traktItem.episode?.ids.getIDs()
        }
        else {
            self.title = ""
            self.traktType = .movie
        }
        totalCount = 0
    }

    init(listItem:TraktListItem) {
        if let movie = listItem.movie {
            self.traktType = .movie
            self.title = movie.title
            self.rootIDs = movie.ids.getIDs()
            self.year = movie.year
            if let rating = movie.rating {
                self.rating?.trakt = "\(Int(rating * 10)) %"
            }
        }
        else if let episode = listItem.episode {
            self.title = episode.title ?? "\(episode.number) - \(String(localized: .episode))"
            self.traktType = .episode
            self.episode = episode.number
            self.season = episode.season
            if let rating = episode.rating {
                self.rating?.trakt = "\(Int(rating * 10))%"
            }
            //IDs
            self.rootIDs = listItem.show?.ids.getIDs()
            self.seasonIDs = listItem.season?.ids.getIDs()
            self.episodeIDs = listItem.episode?.ids.getIDs()
        }
        else if let show = listItem.show {
            self.traktType = .tvshow
            self.title = show.title
            self.rootIDs = show.ids.getIDs()
            self.year = show.year
            if let rating = show.rating {
                self.rating?.trakt = "\(Int(rating * 10)) %"
            }
        }
        else {
            self.title = ""
            self.traktType = .movie
        }
        totalCount = 0
    }
    
    public static func == (lhs: SCCMovie, rhs: SCCMovie) -> Bool {
        switch lhs.traktType {
        case .tvshow,
             .movie:
            return lhs.rootIDs == rhs.rootIDs
        case .episode:
            return lhs.episodeIDs == rhs.episodeIDs
        case .season:
            return lhs.seasonIDs == rhs.seasonIDs
        }
    }
}
