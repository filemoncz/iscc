//
//  GenreTableCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

protocol GenreTableCellDelegate: AnyObject {
    func genreTableCell(_ cell:GenreTableCell, didSelect movie:SCCMovie)
    func genreTableCell(_ cell:GenreTableCell, dataFor value:FindingValue, startIndex: Int)
}

protocol GenreTableCellFocusDelegate: AnyObject {
    func genreTableCell(_ cell:GenreTableCell, didFocus index:Int)
    func focusForIndexPathGenreCell(_ cell:GenreTableCell) -> IndexPath?
}

final class GenreTableCell: UITableViewCell {
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    public weak var delegate:GenreTableCellDelegate?
    public weak var focusDelegate: GenreTableCellFocusDelegate?
    private(set) var genre: FindingValue?
    private var model: SCCMovieResult? {
        didSet {
            self.collectionView?.reloadData()
        }
    }
    private var filterType: FilterType = .movie
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func configure() {
        if self.collectionView.delegate == nil {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.alwaysBounceHorizontal = true
            self.collectionView.register(UINib(nibName: "MovieCell", bundle: nil),
                                         forCellWithReuseIdentifier: "MovieCell")
            self.collectionView.backgroundColor = .clear
        }
    }
    
    public func cell(for genre: FindingValue, type: FilterType, model: SCCMovieResult?, indexPath: IndexPath) {
        self.configure()
        self.model = model
        self.filterType = type
        self.genre = genre
        self.sectionTitleLabel.text = genre.string
        self.fetchData(for: genre)
        self.collectionView.remembersLastFocusedIndexPath = true
        self.collectionView.setNeedsFocusUpdate()
        self.collectionView.updateFocusIfNeeded()
    }
    
    public func update(model: SCCMovieResult?, for value: FindingValue) {
        if let value = value as? Genere,
            let genere = self.genre as? Genere,
            value == genere
        {
            self.model = model
        }
        else if
            let value = value as? Friend,
            let friend = self.genre as? Friend,
                value == friend
        {
            self.model = model
        }
    }
    
    private func fetchData(for genre:FindingValue, startIndex: Int = 0) {
        self.delegate?.genreTableCell(self, dataFor: genre, startIndex: startIndex)
    }
}

extension GenreTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = self.model?.data.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as? MovieCell {
            if let data = self.model?.data[indexPath.item] {
                cell.configure(data)
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let model = self.model,
           indexPath.row == model.data.count - 1,
            model.isExistsNextData,
           let genre = self.genre {
            self.fetchData(for: genre, startIndex: model.data.count)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let data = self.model?.data,
           data.count > indexPath.row {
            self.delegate?.genreTableCell(self, didSelect: data[indexPath.row])
        }
    }
    
    func indexPathForPreferredFocusedView(in collectionView: UICollectionView) -> IndexPath? {
        return self.focusDelegate?.focusForIndexPathGenreCell(self)
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let nextItem = context.nextFocusedItem as? MovieCell,
           let lastIndexPath = collectionView.indexPath(for: nextItem) {
            self.focusDelegate?.genreTableCell(self, didFocus: lastIndexPath.row)
        }
    }
}
