//
//  GenreTableView.swift
//  StreamCinema.atv
//
//  Created by SCC on 22/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit
import Combine

protocol GenreViewModelDelegate: AnyObject {
    func genreViewModel(_ viewModel: SpecialMovieTableViewModel, didSelect movie:SCCMovie)
    func genreViewModel(_ viewModel: SpecialMovieTableViewModel, indexPath cell: GenreTableCell) -> Int?
}

protocol SpecialMovieTableViewModel {
    var type: FilterType { get set }
    var genreDelegate:GenreViewModelDelegate? { get set }
    var errorSubject: PassthroughSubject<Error, Never> { get set }
    var onNewData: (() -> Void)? { get set }
    
    func getNumberOfRows(in section:Int) -> Int
    func getCell(forRowAt indexPath:IndexPath, in tableView:UITableView) -> UITableViewCell
}

final class Friend: Codable, Hashable, FindingValue {
    var name: String
    var alias: String
    var hasnedName: String
    var sccIDs: [String] = []
    var position: Int
    
    var value: String {
        hasnedName
    }
    
    var string: String {
        alias
    }
    
    init(name: String, alias: String, hasnedName: String, sortedPosition: Int, sccIDs: [String] = []) {
        self.name = name
        self.alias = alias
        self.hasnedName = hasnedName
        self.sccIDs = sccIDs
        self.position = sortedPosition
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.alias, forKey: .alias)
        try container.encode(self.hasnedName, forKey: .hasnedName)
        try container.encode(self.sccIDs, forKey: .sccIDs)
        try container.encode(self.position, forKey: .position)
    }
    
    enum CodingKeys: CodingKey {
        case name
        case alias
        case hasnedName
        case sccIDs
        case position
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.alias = try container.decode(String.self, forKey: .alias)
        self.hasnedName = try container.decode(String.self, forKey: .hasnedName)
        self.sccIDs = try container.decode([String].self, forKey: .sccIDs)
        self.position = try container.decode(Int.self, forKey: .position)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
    
    static func == (lhs: Friend, rhs: Friend) -> Bool {
        lhs.name == rhs.name
    }
}

final class FriendData {
    static var friends: [Friend] {
        get {
            do {
                if let data = UserDefaults.standard.value(forKey: "FirendData.friends") as? Data
                {
                    let myObject = try PropertyListDecoder().decode([Friend].self, from: data)
                    return myObject
                }
            } catch  {
                print("Reading friend list: \(error)")
            }
            return []
        }
        
        set {
            let value = try? PropertyListEncoder().encode(newValue)
            UserDefaults.standard.set(value, forKey: "FirendData.friends")
        }
    }
}

enum FriendsModelState {
    case prepare
    case readinFriendsData
    case readingSccData
    case allDataStored
}

// MARK: - FriendsModel
final class FriendsModel: SpecialMovieTableViewModel {
    var type: FilterType
    weak var genreDelegate: GenreViewModelDelegate?
    var errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    var onNewData: (() -> Void)?
    let appData: AppData
    private var friends:[Friend] = []
    private var modelData: [Friend:SCCMovieResult] = [:]
    private var focusItems: [Int:Int] = [:]
    private var state:FriendsModelState = .prepare
    
    init(appData: AppData, type: FilterType) {
        self.appData = appData
        self.type = type
        fetchFriendsData()
    }
    
    func getNumberOfRows(in section: Int) -> Int {
        return friends.count
    }
    
    func getCell(forRowAt indexPath: IndexPath, in tableView: UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GenreTableCell") as? GenreTableCell {
            let genre = friends[indexPath.row]
            let result = self.modelData[genre]
            cell.delegate = self
            cell.cell(for:genre, type: self.type ,model: result, indexPath: indexPath)
            cell.focusDelegate = self
            cell.selectionStyle = .none
            cell.focusStyle = .custom
            return cell
        }
        return UITableViewCell()
    }
    
    
    private func fetchData(for genre:FindingValue, startIndex: Int, cell: GenreTableCell) {
        
    }
    
    private func fetchFriendsData() {
        let allFriends = FriendData.friends
        guard allFriends.count > 0 else { return }
        let type = self.type
        Task {
            for i in 0...allFriends.count - 1 {
                state = .readinFriendsData
                var movies = try await appData.isccService.asyncShared(from: [allFriends[i].value], type: type)
                let date = Date()
                movies.sort(by: { $0.dateAdded ?? date > $1.dateAdded ?? date })
                allFriends[i].sccIDs = movies.filter({ $0.isRecommend }).compactMap({ $0.sccID })
                
                if allFriends[i].sccIDs.count > 0 {
                    self.friends.append(allFriends[i])
                    state = .readingSccData
                    let resultModel = try await appData.scService.getMediaAsync(by: allFriends[i].sccIDs,
                                                                                type: type,
                                                                                startIndex: 0,
                                                                                offSet: 100)
                    self.modelData[allFriends[i]] = resultModel
                }
            }
            DispatchQueue.main.async { [weak self] in
                self?.onNewData?()
            }
        }
    }
}

extension FriendsModel: GenreTableCellFocusDelegate {
    func genreTableCell(_ cell: GenreTableCell, didFocus index: Int) {
        
    }
    
    func focusForIndexPathGenreCell(_ cell: GenreTableCell) -> IndexPath? {
        return nil
    }
}

extension FriendsModel: GenreTableCellDelegate {
    func genreTableCell(_ cell: GenreTableCell, didSelect movie: SCCMovie) {
        genreDelegate?.genreViewModel(self, didSelect: movie)
    }
    
    func genreTableCell(_ cell: GenreTableCell, dataFor value: FindingValue, startIndex: Int) {
        fetchData(for: value, startIndex: startIndex, cell: cell)
    }
}

// MARK: - GenreViewModel
final class GenreViewModel: SpecialMovieTableViewModel {

    private var model:[Genere] {
        get {
            var generes = Genere.allTvShows
            if self.type == .movie {
                generes = Genere.allMovie
            }
            if let isDisabled = (UIApplication.shared.delegate as? AppDelegate)?.appData.isDisableAdult,
               isDisabled == true {
                generes = generes.filter { genere -> Bool in
                    if Genere.explicit.contains(genere) {
                        return false
                    }
                    return true
                }
            }
            return generes
        }
    }
    private var modelData: [Genere:SCCMovieResult] = [:]
    private var focusItems: [Int:Int] = [:]

    private let appData: AppData
    public var type: FilterType = .movie
    public weak var genreDelegate:GenreViewModelDelegate?
    var onNewData: (() -> Void)?

    var errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()

    init(appData: AppData) {
        self.appData = appData
    }

    public func getNumberOfRows(in section:Int) -> Int {
        return self.model.count
    }
    
    public func getCell(forRowAt indexPath:IndexPath, in tableView:UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "GenreTableCell") as? GenreTableCell {
            let genre = self.model[indexPath.row]
            let result = self.modelData[genre]
            cell.delegate = self
            cell.cell(for:genre, type: self.type ,model: result, indexPath: indexPath)
            cell.focusDelegate = self
            cell.selectionStyle = .none
            cell.focusStyle = .custom
            return cell
        }
        return UITableViewCell()
    }
    
    private func fetchData(for genre:FindingValue, startIndex: Int, cell: GenreTableCell) {
        appData.scService
            .genre(with: genre, type: type, startIndex: startIndex)
            .assignError(to: errorSubject)
            .sink { [weak self] model in
                self?.update(data: model, genre: genre as! Genere, startIndex: startIndex, cell: cell)
            }.store(in: &cancelables)
    }
    
    private func update(data:SCCMovieResult, genre:Genere, startIndex: Int, cell: GenreTableCell) {
        if startIndex == 0 {
            self.modelData[genre] = data
        } else {
            self.modelData[genre]?.data.append(contentsOf: data.data)
        }
        cell.update(model: modelData[genre], for: genre)
        cell.collectionView.reloadData()
    }
}

extension GenreViewModel: GenreTableCellFocusDelegate {
    func focusForIndexPathGenreCell(_ cell: GenreTableCell) -> IndexPath? {
        guard let cellIndex = self.genreDelegate?.genreViewModel(self, indexPath: cell),
              let row = self.focusItems[cellIndex] else { return  nil}
        return IndexPath(row: row, section: 0)
    }
    
    func genreTableCell(_ cell: GenreTableCell, didFocus index: Int) {
        guard let cellIndex = self.genreDelegate?.genreViewModel(self, indexPath: cell) else { return }
        self.focusItems[cellIndex] = index
    }
}

extension GenreViewModel: GenreTableCellDelegate {
    func genreTableCell(_ cell: GenreTableCell, dataFor value: FindingValue, startIndex: Int) {
        self.fetchData(for: value, startIndex: startIndex, cell: cell)
    }
    
    func genreTableCell(_ cell: GenreTableCell, didSelect movie: SCCMovie) {
        self.genreDelegate?.genreViewModel(self, didSelect: movie)
    }
}

// MARK: - tableView
protocol GenreTableDelegate: AnyObject {
    func castCollectionView(_ castView:GenreTableView, didSelect movie:SCCMovie)
}

final class GenreTableView: UITableView {

    static func create(appData: AppData, item:MenuItem, frame: CGRect, style: UITableView.Style, type: FilterType?) -> GenreTableView {
        let view = GenreTableView(frame: frame, style: style)
        view.appData = appData
        if item == .genre {
            view.model =  GenreViewModel(appData: appData)
        } else if item == .friendsAdvise {
            view.model =  FriendsModel(appData: appData, type: type ?? .movie)
        }
        view.configure()
        return view
    }

    private let bacgroundTextLabel = UILabel()
    public weak var genreDelegate:GenreTableDelegate?
    private var appData: AppData!
    private var model: SpecialMovieTableViewModel!
    let errorSubject: PassthroughSubject<Error, Never> = PassthroughSubject()
    private var cancelables: Set<AnyCancellable> = Set()
    
    public var type: FilterType = .movie {
        didSet {
            self.model.type = self.type
            model.onNewData = { [weak self] in
                self?.reloadData()
            }
        }
    }
    
    var bacgroundText: String? {
        get {
            bacgroundTextLabel.text
        }
        set {
            bacgroundTextLabel.text = newValue
        }
    }
    
    private func configure() {
        self.model.genreDelegate = self
        self.delegate = self
        self.dataSource = self
        self.register(UINib(nibName: "GenreTableCell", bundle: nil), forCellReuseIdentifier: "GenreTableCell")
        self.rowHeight = UITableView.automaticDimension
        backgroundView = bacgroundTextLabel
        bacgroundTextLabel.textAlignment = .center
        bacgroundTextLabel.numberOfLines = 0
        setupSubcriptions()
    }

    private func setupSubcriptions() {
        model
            .errorSubject
            .sink { (error) in
                self.errorSubject.send(error)
            }.store(in: &cancelables)
    }

}

extension GenreTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 640
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.getNumberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.model.getCell(forRowAt: indexPath, in: tableView)
    }
    
    func tableView(_ tableView: UITableView, canFocusRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

extension GenreTableView: GenreViewModelDelegate {
    func genreViewModel(_ viewModel: SpecialMovieTableViewModel, didSelect movie: SCCMovie) {
        self.genreDelegate?.castCollectionView(self, didSelect: movie)
    }
    
    func genreViewModel(_ viewModel: SpecialMovieTableViewModel, indexPath cell: GenreTableCell) -> Int? {
        return self.indexPath(for: cell)?.row
    }
}
