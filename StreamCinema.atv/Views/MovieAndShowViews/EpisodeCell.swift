//
//  MovieCell.swift
//  MovieDBTV
//
//  Created by Alfian Losari on 23/03/19.
//  Copyright © 2019 Alfian Losari. All rights reserved.
//

import UIKit
import Kingfisher

final class EpisodeCell: UICollectionViewCell {
    
    @IBOutlet var watchedImage: UIImageView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    
    @IBOutlet var unfocusedConstraint: NSLayoutConstraint!
    var focusedConstraint: NSLayoutConstraint!
    
    public var data:SCCMovie?
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        
        return formatter
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
#if canImport(TVUIKit)
        self.focusedConstraint = self.titleLabel.topAnchor.constraint(equalTo: self.imageView.focusedFrameGuide.bottomAnchor,
                                                                      constant: 4)
        #endif
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        self.focusedConstraint?.isActive = isFocused
        self.unfocusedConstraint?.isActive = !isFocused
    }
    
    private func isWatched(_ state: WatchedState?) {
        let watchedState = state ?? .none
        self.watchedImage.isHidden = watchedState == .none
        self.watchedImage.layer.shadowColor = UIColor.darkGray.cgColor
        self.watchedImage.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.watchedImage.layer.shadowOpacity = 1
        self.watchedImage.layer.shadowRadius = 1.0
        self.watchedImage.clipsToBounds = false
        
        var image: UIImage? = nil
        if watchedState == .done {
            image = UIImage(systemName: "checkmark.circle.fill")
        } else if watchedState == .paused {
            image = UIImage(systemName: "pause.circle.fill")
        } else if watchedState == .trakt {
            image = UIImage(named: "traktLogo")
        }
        let config = UIImage.SymbolConfiguration(paletteColors: [.white, .systemMint])
        
        self.watchedImage.image = image?.applyingSymbolConfiguration(config)
    }
    
    public func set(movie: SCCMovie) {
        self.titleLabel.text = movie.title
        
        if let posterUrl = movie.poster {
            self.imageView.setCashedImage(url: posterUrl, type: .poster)
        } else {
            self.imageView.image = UIImage(named: "poster")
        }
        
        if let rating = movie.rating?.ratingTitle() {
            self.ratingLabel.text = rating
            self.ratingLabel.isHidden = false
        } else {
            self.ratingLabel.isHidden = true
        }
        
        self.isWatched(movie.state)
    }
    
    func configure(_ movie: SCCMovie) {
        self.data = movie
        imageView.kf.indicatorType = .none
        if let poster = movie.poster {
            self.imageView.setCashedImage(url: poster, type: .poster)
        } else {
            self.imageView.image = UIImage(named: "poster")
        }
        
        let string:NSMutableAttributedString = NSMutableAttributedString(string: movie.title + " ")

        ratingLabel.textColor = .secondaryLabel
        if let rating = movie.rating {
            ratingLabel.text = rating.ratingTitle()
            ratingLabel.isHidden = false
        } else {
            ratingLabel.isHidden = true
        }
        
        if let streams = movie.auiodLangs {
            let audioString = NSMutableAttributedString(string: streams.joined(separator: ", "))
            let range = NSRange(location: 0, length: audioString.length)
            audioString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
            
            string.append(NSAttributedString(string: "\n"))
            string.append(audioString)
        }
        
        titleLabel.attributedText = string
        self.isWatched(movie.state)
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        
        setNeedsUpdateConstraints()
        coordinator.addCoordinatedAnimations({
            self.layoutIfNeeded()
        }, completion: nil)
    }    
}
