//
//  StudiosCellView.swift
//  StreamCinema.atv
//
//  Created by iSCC on 23/12/2022.
//  Copyright © 2022 SCC. All rights reserved.
//

import UIKit
import TMDBKit
import Kingfisher

protocol StudiosCellViewDelegate: AnyObject {
    func studiosCellView(_ view: StudiosCellView, didFound url:URL)
}

final class StudiosCellView: UICollectionViewCell {
    weak var delegate: StudiosCellViewDelegate?
    
    internal let imageBGView = UIView()
    internal let imageView = UIImageView()
    internal let studioNameLabel = UILabel()
    private(set) var data:AZResult?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func configureSubviews() {
        addSubview(imageBGView)
        imageBGView.translatesAutoresizingMaskIntoConstraints = false
        
        imageBGView.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        imageBGView.rightAnchor.constraint(equalTo: rightAnchor, constant:  -16).isActive = true
        imageBGView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        imageBGView.heightAnchor.constraint(equalToConstant: 225).isActive = true
        
        imageBGView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.topAnchor.constraint(equalTo: imageBGView.topAnchor, constant: 8).isActive = true
        imageView.rightAnchor.constraint(equalTo: imageBGView.rightAnchor, constant:  -8).isActive = true
        imageView.leftAnchor.constraint(equalTo: imageBGView.leftAnchor, constant: 8).isActive = true
        imageView.bottomAnchor.constraint(equalTo: imageBGView.bottomAnchor, constant: -8).isActive = true
        
        addSubview(studioNameLabel)
        studioNameLabel.translatesAutoresizingMaskIntoConstraints = false
        
        studioNameLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
        studioNameLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        studioNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant:  -8).isActive = true
        studioNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        
        studioNameLabel.textColor = .white.withAlphaComponent(0.7)
        studioNameLabel.textAlignment = .center
        studioNameLabel.adjustsFontSizeToFitWidth = true
        studioNameLabel.minimumScaleFactor = 0.1
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        
        
        imageBGView.clipsToBounds = true
        imageBGView.layer.cornerRadius = 8
        imageBGView.backgroundColor = .white.withAlphaComponent(0.2)
    }
    
    func set(_ data: AZResult) {
        self.data = data
        studioNameLabel.text = "\(data.value ?? "NoName")"
        imageView.image = nil
        if let url = data.imageUrl {
            loadImage(from: url)
        } else {
            findImageForCurrentResult()
        }
    }
    
    internal func loadImage(from url: URL) {
        imageView.kf.indicatorType = .none
        imageView.kf.setImage(
            with: url,
            placeholder: nil,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(0.5))
            ]
        )
    }
    
    internal func findImageForCurrentResult() {
        guard let studioName = data?.value else { return }
        SearchMDB.company(query: studioName, page: nil) { [weak self] clientReturn, company in
            guard let self,
                    let company = company?.filter({ $0.logo_path != nil }).first,
                    let logoPathUrl = URL(string: "https://image.tmdb.org/t/p/w500\(company.logo_path!)")
            else { return }
            self.loadImage(from: logoPathUrl)
            self.data?.imageUrl = logoPathUrl
            self.delegate?.studiosCellView(self, didFound: logoPathUrl)
        }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        UIView.animate(withDuration: 0.2) {
            if (context.nextFocusedView == self) {
                let trans = self.contentView.transform.scaledBy(x: 1.1, y: 1.1);
                self.transform = trans;
            } else {
                let trans = self.contentView.transform.scaledBy(x: 1, y: 1);
                self.transform = trans;
            }
        }

        coordinator.addCoordinatedAnimations({ [unowned self] in
            if self.isFocused {
                let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
                verticalMotionEffect.minimumRelativeValue = -10
                verticalMotionEffect.maximumRelativeValue = 10

                let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
                horizontalMotionEffect.minimumRelativeValue = -10
                horizontalMotionEffect.maximumRelativeValue = 10

                let motionEffectGroup = UIMotionEffectGroup()
                motionEffectGroup.motionEffects = [horizontalMotionEffect, verticalMotionEffect]

                self.addMotionEffect(motionEffectGroup)
            }
            else
            {
                for efect in self.motionEffects {
                    self.removeMotionEffect(efect)
                }
            }
            },
        completion: nil)
    }
}


