//
//  TVStationCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 28/11/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import UIKit

final class TVStationCell: UITableViewCell {
    @IBOutlet weak var stationPincon: UIImageView!
    @IBOutlet weak var currentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var nextTitle: UILabel!
    @IBOutlet weak var nextDurationLabel: UILabel!
    
    
    func setProgress(start:Date, stop:Date, current:Date, nextStart:Date?, nextEnd:Date?) {
        self.titleLabel.isHidden = false
        self.durationLabel.isHidden = false
        self.progressView.isHidden = false
        self.nextTitle.isHidden = false
        
        
        let startTimeStamp = start.timeIntervalSince1970
        let stopTimeStamp = stop.timeIntervalSince1970
        let currentTimeStamp = current.timeIntervalSince1970
        
        let duration = stopTimeStamp - startTimeStamp
        let currentTime = currentTimeStamp - startTimeStamp
        
        let position = currentTime / duration
        self.progressView.progress = Float(position)
        
        if let startTimeString = start.toTime, let stopTimeString = stop.toTime {
            self.durationLabel.text = startTimeString + " - " + stopTimeString
        }
        
        if let nextStart = nextStart?.toTime, let nextEnd = nextEnd?.toTime {
            self.nextDurationLabel.text = nextStart + " - " + nextEnd
            self.nextDurationLabel.isHidden = false
        }
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        super.didUpdateFocus(in: context, with: coordinator)
        if context.nextFocusedView === self {
            coordinator.addCoordinatedAnimations({
                self.titleLabel.textColor = .lightGray
                self.durationLabel.textColor = .lightGray
                self.progressView.tintColor = .lightGray
                self.nextTitle.textColor = .lightGray
                self.nextDurationLabel.textColor = .lightGray
            }, completion: nil)
        }
        else {
            coordinator.addCoordinatedAnimations({
                self.titleLabel.textColor = .label
                self.durationLabel.textColor = .label
                self.progressView.tintColor = .label
                self.nextTitle.textColor = .label
                self.nextDurationLabel.textColor = .label
            }, completion: nil)
        }
    }
}
