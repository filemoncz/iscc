//
//  MovieHorizontalCell.swift
//  StreamCinema.atv
//
//  Created by SCC on 19/01/2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import Kingfisher

final class MovieHorizontalCell: UICollectionViewCell {
    @IBOutlet weak var bacgroundImageView: UIImageView!
    @IBOutlet weak var gradiantView: UIView!
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet weak var langLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    public func set(_ model:SCCMovie) {
        self.bacgroundImageView.image = UIImage(named: "fanart")
        model.cellBacgroundImage { url in
            if let url = url {
                self.loadImage(at: url)
            }
        }
        if let rating = model.rating?.ratingTitle() {
            self.ratingLabel.text = rating
            self.ratingLabel.isHidden = false
        } else {
            self.ratingLabel.isHidden = true
        }
        if let langs = model.auiodLangs {
            self.langLabel.text = " " + langs.joined(separator: ", ") + " "
            self.langLabel.isHidden = false
        } else {
            self.langLabel.isHidden = true
        }
        self.titleLabel.text = model.title
    }
    
    private func loadImage(at url:URL) {
        self.bacgroundImageView.kf.indicatorType = .none
        self.bacgroundImageView.setCashedImage(url: url, type: .emptyLoading)
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        UIView.animate(withDuration: 0.2) {
            if (context.nextFocusedView == self) {
                let trans = self.contentView.transform.scaledBy(x: 1.1, y: 1.1);
                self.transform = trans;
            } else {
                let trans = self.contentView.transform.scaledBy(x: 1, y: 1);
                self.transform = trans;
            }
        }
        
        coordinator.addCoordinatedAnimations({ [unowned self] in
            if self.isFocused {
                let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
                verticalMotionEffect.minimumRelativeValue = -10
                verticalMotionEffect.maximumRelativeValue = 10

                let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
                horizontalMotionEffect.minimumRelativeValue = -10
                horizontalMotionEffect.maximumRelativeValue = 10

                let motionEffectGroup = UIMotionEffectGroup()
                motionEffectGroup.motionEffects = [horizontalMotionEffect, verticalMotionEffect]

                self.addMotionEffect(motionEffectGroup)
            }
            else
            {
                for efect in self.motionEffects {
                    self.removeMotionEffect(efect)
                }
            }
            },
        completion: nil)
    }
}
