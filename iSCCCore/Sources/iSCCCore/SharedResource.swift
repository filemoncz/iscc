//
//  File.swift
//  
//
//  Created by Martin Matějka on 13.01.2021.
//

import SwiftUI

#if os(iOS)
import UIKit
#endif

// SharedResource.swift
import Foundation

public enum Resource {

    public enum Color: String {
        case defaultGreen
    }
    
    public enum Image: String {
        case onboardingBacground
        case fanart
    }

    public enum Text: String {
        case tvShows = "TvShows"
        case movies = "Movies"
        case movies2 = "movies"
        case genre = "genre"
        case year = "year"
        case loginButton = "button_logon"
        case concerts = "concerts"
        case playButton = "button_play"
        case playHDRButton = "button_HDR_play"
        case continuePlayButton = "button_continuePlaying"
        case trailerButton = "button_trailer"
        case disabled = "disabled"
        case series = "series"
        case search = "search"
        case artist = "artist"
        case staff = "staff"
        case studios = "studios"
        case writers = "writers"
        case directors = "directors"
        case related = "releated"
        case az_Search = "az_search"
        case settings = "settings"
        case popular = "popular"
        case most_watched = "most_watched"
        case new_releases = "new-releases"
        case new_dubbed_releases = "new-dubbed-releases"
        case recently_added = "recently-added"
        case recently_dubbed_added = "recently-dubbed-added"
        case watch_history = "watch_history"
        case christmas = "christmas"
        case friendsAdvise = "friendsAdvise"
        case watch_list = "watch_list"
        case provider_details = "provider-details"
        case provider_settings = "provider-settings"
        case webshare_credentials = "webshare-credentials"
        case trakt_credentials = "trakt-credentials"
        case opensubtitles_credentials = "opensubtitles-credentials"
        case subtitles_menu_item = "subtitles-menu-item"
        case subtitles_font_setting = "subtitles-font-settings"
        case subtitles_font_size = "subtitles-font-size"
        case subtitles_font_size_small = "subtitles-font-size-small"
        case subtitles_font_size_normal = "subtitles-font-size-normal"
        case subtitles_font_size_large = "subtitles-font-size-large"
        case subtitles_font_size_larger = "subtitles-font-size-larger"
        case firends_add = "firends_add"
        case firends_remove = "firends_remove"
        case firends_list = "firends_list"
        case username = "username"
        case alias = "alias"
        case position = "position"
        case password = "password"
        case button_changeCredentials = "button_changeCredentials"
        case remaining_days_of_provider_subs = "remaining-days-of-provider-subscription"
        case to_watch_the_content_you_need_vip = "to-watch-the-content-you-need-to-have-a-vip-accoun"
        case missing_credentials = "missing-provider-credentials"
        case button_save = "button_save"
        case button_continue = "button_continue"
        case button_sort = "button_sort"
        case bad_credentials = "bad_credentials"
        case logoutButton = "button_logout"
        case logonScreenTitle = "title_logonScreen"
        case buttonCancel = "button_cancel"
        case chooseStream = "choose-the-stream"
        case season = "season"
        case episode = "episode"
        case setting_in_progress = "Settings_WorkInProgress"
        case setting_delete_data_title = "settings_delete_app_data_title"
        case setting_delete_data_message = "settings_delete_app_data_message"
        case buttonDelete = "button_delete"
        case buttonReset = "button_reset"
        case buttonOK = "button_ok"
        case setting_other_app = "other_app_settings"
        case info = "Info"
        case subtitles = "subtitles"
        case audio = "Audio"
        case track = "track"
        case no_audio = "No Audio"
        case sound = "sound"
        case delay = "delay"
        case full_dynamic_range = "Full Dynamic Range"
        case reduce_loud_sounds = "Reduce Loud Sounds"
        case buttonStartSpeed = "button_startSpeed"
        case last_speedTest_resutl = "last_speedTest_result"
        case button_resetWatched = "button_resetWatched"
        case trakt_qr_text = "trakt_qr_text"
        case trakt = "trakt"
        case trending = "trending"
        case tvProgram = "tvProgram"
        case isVIP = "is_VIP"
        case TRUE = "true"
        case FALSE = "false"
        case new_version = "new_version"
        case new_version_messsage = "new_version_message"
        case duration = "duration"
        case genere = "genere"
        case views = "views"
        case lang = "lang"
        case sub = "sub"
        case rating = "rating"
        case action = "Action"
        case animated = "Animated"
        case adventure = "Adventure"
        case documentary = "Documentary"
        case drame = "Drama"
        case erotic = "Erotic"
        case fantazy = "Fantasy"
        case historical = "Historical"
        case horror = "Horror"
        case music = "Music"
        case imax = "IMAX"
        case catastrophic = "Catastrophic"
        case comedy = "Comedy"
        case short = "Short"
        case crime = "Crime"
        case musical = "Musical"
        case mystery = "Mystery"
        case educational = "Educational"
        case fairytale = "Fairytale"
        case pornographic = "Pornographic"
        case psychological = "Psychological"
        case journalistic = "Journalistic"
        case reality = "Reality"
        case travel = "Travel"
        case family = "Family"
        case romance = "Romance"
        case sci_fi = "Sci-Fi"
        case competition = "Competition"
        case sports = "Sports"
        case stand_up = "Stand-Up"
        case telenovela = "Telenovela"
        case thriller = "Thriller"
        case military = "Military"
        case western = "Western"
        case biographical = "Biographical"
        case animation = "Animation"
        case talk_show = "Talk-show"
        case trakt_sync = "trakt_sync"
        case wait = "wait"
        case sync_done = "sync_done"
        case sync_false = "sync_false"
        case error = "error"
        
        case settingsCreateFriend = "create_friend_title"
        case settingsCreateFriendMessage = "create_friend_message"
        case settingsRemoveFriend = "remove_friend_title"
        case settingsRemoveFriendMessage = "remove_friend_message"
        case noFriendsPleaseAddNowInSettings = "noFriendsPleaseAddNowInSettings"
        
        case subtitles_font_color
        case tv = "tv"
        case requestFailed = "requestFailed"
        case meList = "me_lists"
        case meFriends = "me_friends"
        case disableErotic = "disableErotic"
        case enabled = "enabled"
        case settingsErocitcTitle = "settingsErocitcTitle"
        case settingsErocitcMessageDisable = "settingsErocitcMessageDisable"
        case settingsErocitcMessageEnable = "settingsErocitcMessageEnable"
        case reTypePassword = "reTypePassword"
        case addToWatchList = "addToWatchList"
        case removeFromWatchList = "removeFromWatchList"
        case appVersion = "appVersion"
        case sortAlertTitle = "sort_alert"
        case sortAlertText = "sort_msg"

        // MARK: Request errors
        // TODO: Add translation for this keys
        case unauthorized = "unauthorized"
        case decodeError = "decodeError"
        case httpError = "httpError"
        case noDataForRequest = "noDataForRequest"
        case noMovies = "noMovies"


        // TODO: Add translation for this keys
        
    }

    
}

public extension Color {

    init(sharedColor: Resource.Color) {
        self.init(sharedColor.rawValue, bundle: .module)
    }
}

public extension Image {
    
    // SwiftUI load image from asset catalog
    init(sharedImage: Resource.Image) {
        self.init(sharedImage.rawValue, bundle: .module)
    }
    
}

public extension Text {

    init(sharedText: Resource.Text) {
        self.init(LocalizedStringKey(sharedText.rawValue), bundle: .module)
    }
}

public extension Resource.Text {

    var localized: String {
        get {
            let localized = NSLocalizedString(self.rawValue, bundle: .module, comment: "")
            if localized == self.rawValue {
                print("WARRNING: Missed localization: \"\(self)\"=\"\(localized)\";")
            }
            return localized
        }
    }
}

public extension String {

    var localized: String {
        get {
            let localized = NSLocalizedString(self, bundle: .module, comment: "")
            if localized == self {
                print("WARRNING: Missed localization: \"\(self)\"=\"\(localized)\";")
            }
            return localized
        }
    }
}

#if os(tvOS)

extension UIImage {
    
    // UIKit load image from asset catalog
    convenience init?(sharedImage: Resource.Image) {
        self.init(named: sharedImage.rawValue, in: .module, compatibleWith: nil)
    }
    
}

#endif



