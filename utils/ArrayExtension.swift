//
//  ArrayExtension.swift
//  StreamCinema.atv
//
//  Created by SCC on 21/07/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

import Foundation

extension Array {
    func asyncCompactMap<ElementOfResult>(_ transform: (Element) async throws -> ElementOfResult?) async throws -> [ElementOfResult] {
        var elements: [ElementOfResult] = []

        for try await element in GenericAsyncSequence(self) {
            if let result = try await transform(element) {
                elements.append(result)
            }
        }

        return elements
    }
}

struct GenericAsyncSequence<Element>: AsyncSequence {
    typealias AsyncIterator = GenericAsyncIterator<Element>

    private let elements: [Element]

    struct GenericAsyncIterator<Element>: AsyncIteratorProtocol {
        private var elements: [Element]

        init(_ elements: [Element]) {
            self.elements = elements
        }

        mutating func next() async throws -> Element? {
            // Can't do popFirst() without further type constraining.
            if !self.elements.isEmpty {
                return self.elements.removeFirst()
            } else {
                return nil
            }
        }
    }

    init(_ elements: [Element]) {
        self.elements = elements
    }

    func makeAsyncIterator() -> AsyncIterator {
        GenericAsyncIterator(self.elements)
    }
}

extension Array where Element: Hashable {
    var uniques: Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    func filterDuplicates(includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
            var results = [Element]()

            forEach { (element) in
                let existingElements = results.filter {
                    return includeElement(element, $0)
                }
                if existingElements.count == 0 {
                    results.append(element)
                }
            }

            return results
        }
}

public extension Sequence {
    func filterDuplicates(includeElement: (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
            var results = [Element]()

            forEach { (element) in
                let existingElements = results.filter {
                    return includeElement(element, $0)
                }
                if existingElements.count == 0 {
                    results.append(element)
                }
            }

            return results
        
    }
}


