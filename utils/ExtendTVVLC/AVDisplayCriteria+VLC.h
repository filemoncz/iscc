//
//  AVDisplayCriteria+VLC.h
//  StreamCinema.atv
//
//  Created by SCC on 27/12/2020.
//  Copyright © 2020 SCC. All rights reserved.
//

#ifndef AVDisplayCriteria_VLC_h
#define AVDisplayCriteria_VLC_h

#import <AVFoundation/AVDisplayCriteria.h>

@interface AVDisplayCriteria ()
@property(readonly) int videoDynamicRange;
@property(readonly, nonatomic) float refreshRate;
- (id)initWithRefreshRate:(float)arg1 videoDynamicRange:(int)arg2;
@end

#endif /* AVDisplayCriteria_VLC_h */
