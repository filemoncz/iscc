//
//  VLCViewModel.swift
//  StreamCinema.atv
//
//  Created by Martin Matějka on 22.01.2021.
//  Copyright © 2021 SCC. All rights reserved.
//

import UIKit
import Combine

public enum VLCPlayerType {
    case movie
    case liveTV
}

public protocol VLCPlayerViewModelProtocol: AnyObject {
    var type: VLCPlayerType { get set }
}

public final class VLCLiveTVPlayerViewModel: VLCPlayerViewModelProtocol {
    public var type: VLCPlayerType = .liveTV

    var link: URL
    var infoPanelData: InfoPanelData
    
    init(link: URL, infoPanelData: InfoPanelData) {
        self.link = link
        self.infoPanelData = infoPanelData
    }
}


public class VLCPlayerViewModel:VLCPlayerViewModelProtocol {

    // MARK: Nested data types
    enum VLCViewModelError: Error {
        case streamDoesNotHaveIdent
        case missingDataForFetchSubtitles
        case missingDataForFetchStreams
    }
    
    var nowPlayingInfo = [String: Any]()
    public var type: VLCPlayerType = .movie
    var data: SCCMovie
    private var modelData: MovieModel
    var link: URL?
    let appData: AppData
    var infoPanelData: InfoPanelData
    var nextData: SCCMovie?

    var osSubtitles: [OSDownload] = []
    var currentVideoNumber: Int = 0
    var continueTitleInLang: String?
    var continueAudioInLang: String?
    let countDownRepet:Float = 20
    var isContrinue: Bool = false

    #if canImport(TVUIKit)
    var range: DynamicRange = .SDR
    #endif
    
    var numberOfVideos: Int {
        if case .movie = modelData.type { return 1 }
        return modelData.episodes.count
    }


    init(modelData: MovieModel, startAt: Int, isContinue: Bool, appData: AppData) {
        self.appData = appData
        self.modelData = modelData
        self.currentVideoNumber = startAt
        self.isContrinue = isContinue
        self.continueAudioInLang = AudioLanguage.init(audioString: nil).getAudioString()
        if let rootMovie = modelData.rootMovie,
           rootMovie.traktType == .movie {
            self.infoPanelData = rootMovie.infoPanel
            self.data = rootMovie
        } else if let selectedEpisode = modelData.episodes.filter({ $0.episode == modelData.selected.episode }).first {
            self.infoPanelData = selectedEpisode.infoPanel
            self.data = selectedEpisode
        } else {
            fatalError()
        }
        self.prepareNextData()
    }
    
    func prepareNextData() {
        guard let currentEpisode = self.data.episode,
              self.data.traktType == .episode else { return }
        
        if let episodeIndex = self.modelData.episodes.firstIndex(where: { $0.episode == (currentEpisode + 1) }),
           modelData.episodes.count > episodeIndex {
            self.nextData = self.modelData.episodes[safe: episodeIndex]
       }
    }
    
    func updateDataFromNextData() {
        if let nextData = self.nextData {
            self.data = nextData
            self.infoPanelData = nextData.infoPanel
            self.nextData = nil
            if let episode = nextData.episode {
                self.modelData.selected.episode = episode
            }
            self.prepareNextData()
        }
    }

    // Combine solution
    func getLink(for stream: VideoStream) -> AnyPublisher<WSStreamModel, Error> {
        guard let token = LoginManager.tokenHash,
                      let uuid = UIDevice.current.identifierForVendor,
                      let ident = stream.ident else {
            return Fail(error: VLCViewModelError.streamDoesNotHaveIdent).eraseToAnyPublisher()
        }

        return appData.wsFileService
            .getFile(wsToken: token, ident: ident, uuid: uuid)
    }
    
    /// Async solution
    func getAsyncLink(for stream: VideoStream) async throws -> WSStreamModel? {
        guard let token = LoginManager.tokenHash,
              let uuid = await UIDevice.current.identifierForVendor,
              let ident = stream.ident else {
            return nil
        }

        return try await appData.wsFileService
            .getFileAsync(wsToken: token, ident: ident, uuid: uuid)
    }

    private let subtitlesLanguagesIdentifiers = ["sk","cs","en"]

    func tryFetchSubtitles() -> AnyPublisher<[OSDownload], Error> {
        guard LoginManager.osTokenHash != nil,
              let imdb = data.ids?.imdb
        else { return Fail(error: VLCViewModelError.missingDataForFetchSubtitles).eraseToAnyPublisher() }

        return appData.osService
            .find(imdbid: imdb, lang: subtitlesLanguagesIdentifiers)
            .handleEvents(receiveOutput: { [weak self] subtitles in
                self?.osSubtitles = subtitles
            })
            .eraseToAnyPublisher()
    }

    func fetchStreams() -> AnyPublisher<VideoStreams, Error> {
        let sccID = self.data.ids?.sccID
        if let episode = self.modelData.episodes.first(where: { $0.episode == self.modelData.selected.episode }) {
            if episode.season != (self.modelData.selected.season),
               self.modelData.seasons.count != 0 {

                fatalError("Zle oznaceny selected")
            }
        }
        
        guard let movieID = sccID else {
            return Fail(error: VLCViewModelError.missingDataForFetchStreams).eraseToAnyPublisher()
        }
        return appData.scService
            .getStreams(for: movieID)
            .eraseToAnyPublisher()
    }
}
